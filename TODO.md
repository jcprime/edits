* Section 1.3.1 (Research importance of natrolites)
    - "Ideal because simple" words
* Section 3.2.2 (HF Methods)
    - Define Slater determinant
    - Finish full words
* Section 3.6.1 (Initial simulations of the natrolite family)
    - "Choice of functional and basis sets" final word-finishings
* Section 3.7 (Methodology development (code))
    - Midway words
* Section 3.8 (flowchart)
    - Move "yes" on decision arrow to be nearer the original rhombus
* Figure 11 caption
* Figure 13 and 14 both image and captions