%!TEX root = main.tex

\section{Literature Review}
\label{lit_review)}

\subsection{Zeolite classification}
\label{litrev:zeolite_classification}

The classification of a zeolite depends on several factors: framework density, space group, cell parameters, and even idealised chemical formulae.
%The idealised chemical formula, space group, and cell parameters can be useful, \emph{except they may not match with the main topology of their overall grouping, so should be considered carefully!} The framework density gives the number of T atoms per 1000  \r{A}$^{3}$ , and should be expected to reside between 12 and 20 in general for inflexible-framework zeolites~\cite[p.~10]{Baerlocher2001_Atlas}.

However, zeolites are primarily classified based on their framework topology.  This is a set of unique combinations of structural properties, such as their coordination sequence and vertex symbol,~\cite[p.~5]{Baerlocher2001_Atlas} which govern the way in which the aluminosilicate frameworks are composed.  For example, given that zeolite frameworks consist of \ce{TO_{4}} tetrahedra, where T is either Si or Al, and the oxygens are shared ``bridges'' between adjacent tetrahedra, then the coordination sequence indicates the ``spread'' of neighbouring O-bridged T atoms from a given starting T atom.  It can be determined for the $k$-th ``sphere'' of coordination using $N_{k} \le 4 \cdot 3^{k-1}$, where $N_{k}$ is the coordination sequence for that ``sphere'' ($N_{0} = 1$).  Similarly, the corresponding vertex symbol highlights the smallest ring size for each pair of opposite angles in the tetrahedron surrounding the starting T atom.  The combination of these two components alone is enough to distinguish all known zeolite framework types.~\cite[p.~5]{Baerlocher2001_Atlas}

Zeolites can be described by so-called ``secondary building units'' (SBUs), which are topological constructs designed to aid in their classification --- where their primary building units are the \ce{TO_{4}} tetrahedra.  All SBUs have distinct identifying symbols, are non-chiral, contain up to 16 T atoms, and are present in unit cells in whole numbers.~\cite[p.~6-7]{Baerlocher2001_Atlas}

\subsection{The natrolite family}
\label{litrev:natrolite_family_structure}

The natrolite family of zeolites have ordered framework structures, with a Si\stroke{}Al ratio of 1.5.

\subsubsection{Natrolite}
\label{litrev:natrolite}

Natrolite~\cite{Pauling1930,Meier1960} has a framework structure given by the NAT topology,~\cite[p.~204]{Baerlocher2001_Atlas} wherein its idealised cell constants give a tetragonal unit cell --- that is, with cell lengths $a = b \neq c$ and angles $\alpha = \beta = \gamma = 90^{\circ}$ --- and orthorhombic symmetry;~\cite{Meier1960}
% --- that is, with cell lengths $a \neq b \neq c$ and angles $\alpha = \beta = \gamma = 90^{\circ}$
it is a member of the \emph{Fdd2} space group.~\cite[p.~204]{Baerlocher2001_Atlas}

It is composed of \emph{c}-axis-linked 4=1 building units, as shown in \textbf{Figure~\ref{fig:building_units}}, with an empirical formula given by \ce{Na_{2}Al_{2}Si_{3}O_{10} $\cdot$ 2 H_{2}O}, and a full unit cell given by \ce{Na_{16}[Al_{16}Si_{24}O_{80}]$\cdot$ 16 H_{2}O}.  The 4=1 SBU is itself made up of \ce{T_{5}O_{10}}, and when linked along natrolite's c-axis, it produces the framework ``chains'',~\cite{Lee2001} which themselves orientate to form helical channels.

\begin{wrapfigure}[9]{L}[0pt]{0.32\textwidth}
    \centering
    \captionsetup{width=0.3\textwidth}
    \includegraphics[height=4cm]{graphics/4=1_SBU.png}
    \caption{Representation of the 4=1 building unit, as is seen in natrolite.}
    \label{fig:building_units}
\end{wrapfigure}

Sodium is the primary extra-framework cation, and natrolite's natural form is hydrated.  Ordinarily, ``isolated'' solvated sodium ions would be surrounded by six water molecules,~\cite{Rempe2001} but in the confines of natrolite's pores, the formally-charged framework oxygens can replace some of the water molecules; this may introduce some degree of structural distortion around the cations, and sodium usually still requires two water molecules to complement the framework oxygens.~\cite{Meier1960}  Natrolite contains two sodium cations and two water molecules per pore, and each cation is coordinated to both water molecules; the water molecules' positions within the pore are crystallographically equivalent.  The cationic charge requires the oxygens to orientate towards them, and so the structure will distort further, to accommodate the necessary ``shaping'' of the framework atoms and the water molecules around the cations in its pores; the sodium cations prefer to orientate nearer the walls of the channels, whereas the water molecules prefer to be positioned in the larger spaces within them.~\cite{Meier1960,Artioli1984}  The form the local coordination structure around each sodium cation takes is a trigonal prism, where the Na-OH$_{2}$ coordination distance is shorter than the Na-OT coordination distance (T $=$ Si or Al).~\cite{Artioli1986}

The experimentally-derived starting structure used in the model for natrolite in this investigation was taken from~\citet{Stuckenschmidt1993}

\subsubsection{Scolecite}
\label{litrev:scolecite}

Scolecite~\cite{Taylor1933,Falth1979} has an empirical formula given by \ce{CaAl_{2}Si_{3}O_{10}$\cdot$ 3 H_{2}O}, a full unit cell given by \ce{Ca_{8}[Al_{16}Si_{24}O_{80}]$\cdot$ 24 H_{2}O}, and the same NAT topology as natrolite, but with calcium as the extra-framework cation; since calcium ions have twice the charge of sodium ions, half the number of calcium ions are required to compensate for the Al-Si substitution in scolecite than of sodium in natrolite.~\cite{Falth1979,Kvick1985}  Understandably, this in itself introduces a further level of distortion than is present in natrolite,~\cite{Falth1979} and thus its symmetry is lowered to monoclinic --- that is, with cell lengths $a \neq b \neq c$, two 90$^{\circ}$ angles, and one non-90$^{\circ}$ angle~\cite{Kvick1985,Yamazaki1989} --- and its space group to \emph{Cc}.  Calcium ions are also ordinarily hydrated, surrounded by seven water molecules;~\cite{Megyes2004,Zavitsas2005} in scolecite's pores, three of these are retained, on average, with four framework oxygens completing the charge compensation.~\cite{Falth1979}

Unlike the sodium cations in natrolite, scolecite's calcium cations prefer to take positions more central to the pore within which they are contained.  The surrounding three water molecules can take crystallographically-inequivalent positions, two of which are close to the cation, and one of which is further away.  The coordination structure around each calcium cation is a distorted pentagonal bipyramid, with the Ca-OH$_{2}$ coordination distance similarly being shorter than the Ca-OT coordination distance.~\cite{Artioli1986}

% \oiface{Need to find Alberti1982 (ALBERTI, A., PONGILUPPI, D. & VEZZALINI, G. (1982). Neues Jahrb. Mineral. Abh. 143, 231-248) to cite alongside Kvick1985 for monoclinic scolecite symmetry.}

The experimentally-derived starting structure used in the model for scolecite in this investigation was taken from~\citet{Stuckenschmidt1997}

\subsubsection{Mesolite}
\label{litrev:mesolite}

Mesolite~\cite{Artioli1986} is also based on the NAT topology, but its generally-accepted structure is not as intuitive.  With an empirical formula given by \ce{Na_{2}Ca_{2}(Al_{2}Si_{3}O_{10})_{3}$\cdot$ 8 H_{2}O}, and a full unit cell formula given by \ce{Na_{16}Ca_{16}(Al_{16}Si_{24}O_{80})_{3}$\cdot$ 64 H_{2}O}, it was named ``meso-'' (``middle'') for its ``intermediate'' nature between the structures of natrolite and scolecite.

The accepted structure of mesolite is composed of ordered layers, as shown in \textbf{Figure~\ref{fig:mesolite_structure}}, alternating one sodium-containing ``natrolite-like'' layer and two calcium-containing ``scolecite-like'' layers, based on X-ray diffraction data.~\cite{Artioli1986}  The alternation of cations from layer to layer introduces a distortion to the framework, changing its overall symmetry to orthogonal,~\cite{Yamazaki1989} with space group \emph{Fdd2}, twisting its chains and shifting the positions of its extra-framework cations and water molecules.~\cite{Artioli1986}  In each distinct layer, the local cation environment is effectively the same as in natrolite and scolecite respectively.

\begin{figure}[ht]
    \centering
    \captionsetup{width=0.8\textwidth}
    \includegraphics[width=0.6\textwidth,align=c]{graphics/mes_gulp/mes_hyd_partial_NEW.png}
    \hspace{1cm}
    \includegraphics[height=5cm,align=c]{graphics/legend/Legend.png}
    \caption{Part of mesolite's unit cell, showing the ``layers'' of natrolite-like and scolecite-like composition; sodium is purple, calcium is green, framework oxygen is red, aluminium is grey, silicon is yellow, water oxygen is dark red, and hydrogen is black.}
    \label{fig:mesolite_structure}
\end{figure}

% Hence the need for computational modelling

% \hide{It is not particularly easy to use experimental techniques to confirm or disprove theoretical structures for such (comparatively) dense microporous materials.  This makes computational modelling an especially viable tool to compensate for the lack of physical knowledge about these specific structures.}

An appropriate model is therefore required that has the best combination of computational efficiency and scientific accuracy, which is what this project is aiming to investigate; the experimentally-derived starting structure used in the model for mesolite was taken from~\citet{Stuckenschmidt2000}.

\subsection{Pressure and temperature effects in zeolites}
\label{litrev:experiment_pressure_temperature}

Many zeolites display some interesting properties when exposed to environmental extremes, such as pressure and temperature; the most pertinent, for this study, are pressure-induced hydration~\cite{Graham2001,Lee2001,Baur2003} and negative thermal expansion.~\cite{Lightfoot2001}  However, much more is currently known about temperature-induced phase transitions than pressure-induced ones.~\cite{Lee2001}

% \oiface{PIH - early physical characterisation~\cite{Moroz2001} and initial diffraction experiments~\cite{Hazen1983}}

% NOTE FROM VIVA: Internal pressure causes the expansion, even though it is induced by an external pressure, because the molecules being pushed into the pores from outside themselves end up pushing on the pore/channel walls, and it is \textit{this} that directly causes the volume expansion

Pressure-induced expansion (PIE) is a phenomenon wherein certain microporous materials expand in volume with increasing pressure; this external pressure around the zeolite forces molecules from its surroundings into the pores, which in turn induces an internal pressure on its pore and channel walls, as a result of this additional ``crowding'' inside the pores.  Pressure-induced hydration (PIH)~\cite{Moroz2001} is the specific variant in which such materials take in water as the pressure being exerted on them increases, due to the abundance of ``empty'' space contained within their frameworks.~\cite{Graham2001,Baur2003}  Such materials, when under high pressures in water-rich environments, are referred to as ``over-'' or ``super-hydrated''.  Natrolite, for example, doubles its capacity for water molecules between external pressures of 0.8 and 1.2~GPa while simultaneously increasing in cell volume;~\cite{Lee2001,Lee2002_PIExpansion} it was the first zeolite experimentally-observed to undergo PIH.~\cite{Belitsky1992,Lee2001}  Several materials have since been shown to undergo PIH: large-pore zeolites,~\cite{Colligan2004} clays,~\cite{You2013} and graphite oxide.~\cite{Talyzin2008}

% NOTE FROM VIVA: NTE is more generally caused by T-induced vibration increases within the frameworks (which would cause expansion in most materials, but everything's bonded into place in zeolites), which induces the T-O-T bonds to start doing this: $T/O\T <=> T\O/T$ more and more vigorously, which itself flattens bonds in the perpendicularplanes, causing the zeolite to ``contract''

Negative thermal expansion (NTE) is effectively the somewhat counterintuitive contraction of a material upon heating.~\cite{Liu2011}  Ordinarily, increasing the temperature of a material will induce an expansion in its volume; however, certain materials, under high temperatures, are reduced in volume.  Many materials that undergo this process are microporous,~\cite{Lightfoot2001} which facilitates the process because their structures can rearrange to accommodate the contraction without losing so much stability that a structural collapse into its fully-dense solid form is more favourable.

% Natrolite, when dehydrated, has been found experimentally to contract along the \emph{a} and \emph{b} axes, and expand (slightly) along the \emph{c} axis; depending on the partial pressure of water in the surroundings, it may take different phase-transition pathways~cite{Wang2010}

\subsection{Hydration and dehydration in the natrolite family}
\label{litrev:dehydration_of_natrolites}

\subsubsection*{Experimental observations}
\label{litrev:expt}

The dehydration behaviour of zeolites in the natrolite family have been deduced experimentally, via thermogravimetric analysis (TGA), differential thermal analysis (DTA), and X-ray diffraction (XRD).

Natrolite has been found to dehydrate in one single ``pairwise'' stage within a single temperature range,~\cite{Peng1955} where the enthalpy of dehydration, $\Delta H_{dehyd}$, is approximately equivalent for every water molecule contained within its pores.  It is therefore understood to contain only one type of water molecule, and so the zeolitic water molecules have a single corresponding bond strength and volatility.  Natrolite breaks down structurally at 940$^{\circ}$C, becoming amorphous,~\cite{Peng1955} whilst below this temperature, (de)hydration is reversible.
% During dehydration, several further phases are formed: paranatrolite --- an intermediately-superhydrated phase, with a water content in between that of fully-hydrated and super-hydrated natrolite --- and metanatrolite --- a fully-dehydrated phase of natrolite. %, and \oiface{tetranatrolite (possibly-discredited structure sharing similarities with gonnardite)}.
There are two distinct phases that fully-dehydrated, so-called ``metanatrolite'' can take, depending on the partial pressure of water at the point of dehydration, known as $\alpha1$- and $\alpha2$-metanatrolite.~\cite{Wang2010}  The former phase, with a monoclinic unit cell and a space group of \emph{F112}, is comparatively more ``contracted'' than the latter, which has an orthorhombic unit cell and a space group of \emph{Fdd2}, due to a greater degree of rotation of the framework tetrahedra, and thus also takes longer to rehydrate than the latter.~\cite{Wang2010}  The sodium cations in dehydrated phases of natrolite occupy different positions to those they took in hydrated natrolite, so as to maximise their coordination to framework oxygens.

Scolecite, however, has been found to dehydrate in two stages, where the water molecules are first lost from a specific site within the pores, cation by cation, and then layer by layer, through the structure; the remaining water molecules are lost at much higher temperatures, according to TGA data.~\cite{Stahl1994_XRD_ScolMes}  It amorphises at a lower temperature than natrolite, at 560$^{\circ}$C,~\cite{Peng1955} undergoing a full structural collapse.

DTA indicates that the second stage of scolecite's dehydration involves water molecules with varying volatilities.~\cite{Peng1955}  Increasing temperature first induces its transformation into a partially-dehydrated phase called metascolecite, where the first eight
% (third of the unit cell's)
water molecules are lost at higher temperatures from the zeolite's pores, all from sites denoted O(2W), and then the remaining 16
% (two-thirds of the unit cell's)
water molecules are lost from sites O(1W) and O(3W), which are neither structurally nor energetically equivalent to those in the previous stage.

There are three known possible water sites in scolecite, denoted O(1W), O(2W), and O(3W), as shown in \textbf{Figure~\ref{fig:scolecite_water_placement}}.  The O(2W) position is located further away from the calcium cation than the other two positions, which would explain the loss of water molecules from these positions first, as they are less tightly-held by the cations.  The O(1W) and O(3W) positions are inequivalent, but are both much closer to the cation than the O(2W) position, and so are not lost as readily with increasing temperature.~\cite{Joswig1984,Cametti2015}

\begin{wrapfigure}[]{R}[0pt]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{graphics/coordination/Scolecite.png}
    \caption{Possible positions for water to take in scolecite; calcium is green, framework oxygens are represented in shades of blue (equivalent positions are the same shade), water molecules are represented as single points in shades of red (equivalent positions are the same shade).}
    \label{fig:scolecite_water_placement}
\end{wrapfigure}

Mesolite has been observed to dehydrate in three stages with increasing temperature, where the calcium-coordinated water molecules are first removed from the scolecite-like ``layers'', then the sodium and calcium ions rearrange to lower their overall energy, and finally the structure undergoes a full framework amorphisation at 490$^{\circ}$C.~\cite{Peng1955}  DTA data supports the suggestion that, given that mesolite's endothermic peaks appear to be coincident with those for both natrolite and scolecite, mesolite's structure holds water molecules in sites
% \oiface{[positions?]}
equivalent to those in natrolite and scolecite, respectively.~\cite{Peng1955}

\begin{wrapfigure}[21]{L}[0pt]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{graphics/coordination/Mesolite_Na.png}
    % \hspace{1cm}
    \includegraphics[height=5cm]{graphics/coordination/Mesolite_Ca.png}
    \caption{Possible positions for water to take around sodium (top) and calcium (above) in mesolite; sodium and calcium are green, framework oxygens are represented in shades of blue (equivalent positions are the same shade), water molecules are represented as single points in shades of red (equivalent positions are the same shade).}
    \label{fig:mesolite_water_placement}
\end{wrapfigure}

\oiface{Copy in the wiggly ($\Delta H$-y) graphs out!  Link to answers.}

\subsection{Computational predictions}
\label{litrev:computational_predictions}

\subsubsection{Interatomic potentials and the natrolite family}
\label{litrev:interatomic_potentials_natrolites}

The natrolite family have been modelled quite successfully using interatomic-potential models.~\cite{Gibbs2004}  However, there has been one consistent problem with these models, and that is in the accuracy of thermodynamic quantities for some members, when compared with experiment.  For example, previous work using interatomic potential models has correctly predicted a linear, ``pairwise'' dehydration process in natrolite, in accordance with experimental findings; however, its volume has not been predicted to contract by as large a proportion as is observed in experiment.
% \oiface{And scolecite is right but $\Delta H_{dehyd}$ rel. wrong?  (Can't read) $\leftarrow$ double peak but $\Delta H$ too exothermic \emph{relative} to natrolite.}
\edit{Additionally, the calculated value for $\Delta H_{dehyd}$ of scolecite, 115.2~$\mathrm{kJ{\cdot}mol^{-1}}$,$\,$\footnote{See Section~\ref{results:nat_sco_gulp} for further information.} is too exothermic relative to that of natrolite, calculated to be 88.9~$\mathrm{kJ{\cdot}mol^{-1}}$,$\,$\footnotemark[\value{footnote}] although the predicted dehydration process is made up of two distinct stages, as is observed experimentally.}

Alteration of the potential parameters for these models has proven to be a somewhat precarious balancing act; when the model is altered to correctly reproduce experiment for the volume changes during dehydration, for example, the agreement of the other parameters with experiment worsens, and vice versa.

% Limitations of IP models for natrolite family so far

Alongside the difficulties faced with the tuning of parameters for the given zeolites, the speed and comparative computational ease with which the simulations can be performed are not necessarily worth the loss of information in the output results.  Ideally, more complex --- and therefore more computationally expensive --- representations could be used to yield more detailed information, and potentially confirm that interatomic potentials are sufficient to obtain the most pertinent data required.

% Water-exploding in GULP~\cite{Gale2003} upon zeolitic hydration attempts, so only dehydration simulations are possible

Additionally, using interatomic potential models to simulate the process of hydration is rather difficult, since hydration of an anhydrous zeolite involves the removal of a hydrated extra-framework cation from its aqueous surroundings and ``pushing'' it --- along with as many water molecules whose coordination it can retain throughout --- into the confines of the zeolitic pores.  The models used are insufficiently robust to complete the hydration process without the structure dissociating due to mutual repulsion at very short separations.

% \oiface{The development of atomistic models for hydrated zeolites; dehydration method when rubbish?}

More robust models will therefore be needed in order to successfully model the hydration of the natrolite family, while the simpler interatomic potential models are sufficient for the dehydration of natrolite and scolecite.  Mesolite's dehydration process is somewhat more complex, and is proving difficult to model using the same parameters as used with natrolite and scolecite, so more complex models may be required to simulate its hydration \emph{and} dehydration.

% Moving originally section 3.4.1 (``Mesolite dehydration'') to below, at Dewi's suggestion:
\subsubsection{Mesolite dehydration}
\label{methodology:mesolite_dehydration}

The calcium ions in mesolite's pores have been shown experimentally to be partially dehydrated first;~\cite{Stahl1994_XRD_Mes} however, simulations so far have predicted the loss of water to come from the sodium ions first.

The highest-energy dehydration pathway for mesolite does predict the loss of calcium-coordinated water molecules before sodium-coordinated ones, but in an order that also fails to correlate with experimental observations; the highest-energy pathway involves the removal of water molecules by pore rather than by position, as shown in \textbf{Figure~\ref{fig:high_energy_mesolite}}.

\begin{figure}
    \includegraphics[width=\textwidth]{graphics/mes_gulp/highenergy/mes05}
    \caption{Mesolite structure after the loss of the first five water molecules, as predicted by the highest-energy pathway in GULP.  Note that while calcium-coordinated water molecules are lost first, they are not lost in layer-by-layer order, instead ``jumping'' from one pore to another in a different layer entirely.}
    \label{fig:high_energy_mesolite}
\end{figure}

\subsubsection{Application of density functional theory to zeolite studies}
\label{litrev:dft_zeolites}

Density functional theory (see \textbf{Section~\ref{theory:dft}}) has successfully been applied to several zeolites in recent years.~\cite{Nicholas1997}  Zeolites studied include silica sodalite,~\cite{Astala2004} chabazite,~\cite{Astala2004} mordenite,~\cite{Astala2004} silica LTA,~\cite{Astala2004} silicalite,~\cite{Astala2004} zeolite Y,~\cite{Niwa2013} MFI-type zeolite,~\cite{Stueckenschneider2012} DOH-type zeolite,~\cite{Smykowski2013} and zeolite Ca-A.~\cite{Arean2009}

The natrolite family has also been the subject of DFT-based investigations.~\cite{Mistry2005_THESIS,Kremleva2013,Kremleva2014,Liu2014,Fischer2015,Hwang2015_Cs-NAT}
% \oiface{WORDS; currently reading p75}~\cite{Mistry2005_THESIS}
The ion-exchange processes in Group I-exchanged natrolites were simulated by Kremleva et al.~(2013), along with the incorporation of water molecules into the pores under pressure, using the projector-augmented wave (PAW) method to represent the core electrons, the PBE exchange-correlation functional, and plane-wave representations of the one-electron wavefunctions; to achieve the simulation of pressure changes, the unit cell shape was varied with constant volume.~\cite{Kremleva2013} % Using VASP
The potassium-exchanged analogue of natrolite has also been studied, by Kremleva et al.~(2014), specifically for its unit cell changes under pressure, in both hydrated and super-hydrated form, using the same parameters.~\cite{Kremleva2014}
Computational simulation has been used, alongside IR and Raman spectroscopy, by Liu et al.~(2014) to consider divalent ion-exchanged natrolites; a plane-wave basis, the PAW approach, and the PBE exchange-correlation functional were also used here, as in the aforementioned studies.~\cite{Liu2014} % Also VASP
Natrolite and scolecite (amongst other zeolites) were studied using DFT by Fischer~(2015), and compared in detail with existing experimental data, with a particular focus on the accuracy of various exchange-correlation functionals on the final output data for the geometry optimisations; plane-wave basis sets and ultrasoft pseudopotentials were employed, and the lattice parameters were fixed while the atomic positions were optimised.  PBE and PW91 functionals were generally found to give the best agreement with experiment, while LDA functionals were insufficient to provide significant agreement, particularly in the lengths of hydrogen bonds.~\cite{Fischer2015} % Using CASTEP; DFT apparently tends to underestimate the length of hydrogen bonds; see ref 2 for stuff about water mobility (large-pore zeolites have more mobile waters than do small-pore zeolites)
% Kremleva2013: useful details and figure of pressure-induced chain rotations

This investigation hopes to contribute towards this existing body of knowledge.

\newpage
