%!TEX root = main.tex

\section{Introduction}
\label{introduction}

\subsection{What are zeolites?}
\label{introduction:what_are_zeolites}

Zeolites are microporous aluminosilicate minerals, with three-dimensional open framework structures, and uniformly-sized pores with diameters of molecular dimensions, up to 20\r{A}.  These properties enable them to act as ``molecular sieves'', and they are thus commercially significant, particularly in areas where selectivity is difficult under typical reaction conditions.

Their frameworks are composed of corner-linked TO$_{4}$ tetrahedra, where T is either (tetravalent) silicon or, typically, (trivalent) aluminium, wherein oxygen atoms act as ``bridges'', shared between adjacent tetrahedra.  The substitution of aluminium, or in some cases other non-tetravalent cations, in place of silicon creates a charge imbalance, with a distribution of negative charge accumulating around the aluminium; zeolites therefore usually require the addition of extra-framework cations (EFCs) --- most commonly protons, forming Br{\o}nsted acid sites,~\cite{Weitkamp2000} or Group 1 (or 2) metal ions --- to neutralise this imbalance.  These EFCs are loosely held within the frameworks, and so can be exchanged with other cations with comparative ease.  This allows for a wider variety of interactions between the zeolite and incoming external molecules, which makes them more useful in such areas as water treatment~\cite{Almalih2015} and petrochemical ``cracking'' processes.~\cite{Yang2003_BOOK}  Non-aluminium substituents have also been used, but this is often reserved for more specific applications; for example, the introduction of certain transition metal atoms allows finer tuning of the respective zeolite's catalytic properties.~\cite{Schoonheydt1993,Corma2003,Godelitsas2003,Smeets2010}

% $[SiO_{4}]^{4-}$ and $[AlO_{4}]^{5-}$

Zeolites are a naturally-metastable morphological form, while their denser, non-porous counterparts are the most stable form under ambient conditions.  They retain their metastable structures during formation due to the environmental conditions under which they were originally formed; natural zeolites are formed as a result of the reaction between silica-containing volcanic ash and groundwater, while synthetic zeolites are often formed using sol-gel processing under controlled conditions, thus allowing far finer control over the structure and properties of the final zeolitic product.

% Dempsey's and Loewenstein's rules

All zeolitic frameworks are composed of primary and secondary building units, the primary ones being the corner-linked \ce{TO_{4}} tetrahedra, and the secondary ones typically taking one of several possible forms.  The geometry of the secondary building units, along wth their positioning relative to one another, dictates the structures of the zeolite's channels and pores; additionally, Loewenstein's and Dempsey's rules guide the possible framework configurations a zeolite is likely to form.

Loewenstein's rule~\cite{Loewenstein1954} states that any given zeolite framework cannot have two adjacent aluminium atoms bridged by an oxygen atom, and so the maximum Si\stroke{}Al ratio for any zeolite is 1:1, wherein every \ce{[AlO_{4}]} tetrahedron is connected via O bridges to no fewer than four \ce{[SiO_{4}]} tetrahedra.  Zeolites with this maximum ratio, particularly when coupled with compensating acidic protons in their frameworks, are particularly well-suited to applications utilising their ion-exchange properties and \ce{[Al^{3+}-O^{-}-Si^{4+}][H^{+}]} Br\o nsted acid sites.~\cite[pp.~81-197]{CatalysisAndZeolites_BOOK}

Dempsey's rule~\cite[p.~293]{Dempsey1968} states that Al-Al interactions in zeolite frameworks will aim to maximise the distance between aluminium atoms, where possible.

These rules --- or rather, guidelines --- can be paired with experimental techniques to relatively successfully deduce a zeolite's structural composition, which has enabled the discovery, characterisation, and analysis of over 200 distinct zeolite topologies.~\cite{IZA_Database,Gaines1998_DANA}

\subsubsection{Zeolite characterisation methods}
\label{introduction:characterisation_methods}

Many techniques can be used to elicit specific details about zeolite structures.  For example, X-ray diffraction (XRD) can yield useful information about the extra-framework cation sites, but tells us very little about the framework atoms.~\cite{Almora-Barrios2001}  Nuclear magnetic resonance (NMR) techniques can be used to provide very short-range details of a zeolite's Si\stroke{}Al distribution,~\cite{Zhao2001} but give no information about the framework longer in range than the first few bonds.~\cite{Almora-Barrios2001}  Thermal gravimetric analysis (TGA) gives details about preferred structures at the temperature(s) at which it is performed, but shows nothing about the mechanism of atomic rearrangement between preferred configurations over the continuous range of temperatures between the two temperatures at which they were tested.

A combination of methods, such as those above, can be used quite effectively to collectively deduce a zeolite's structure, alongside mindfulness of Dempsey's and Loewenstein's rules for framework composition.  However, for dynamic or mechanistic studies, such as hydration and dehydration, the available experimental techniques and the information they yield leave a lot to be desired; computational techniques can therefore compensate in those investigations where experimental techniques fall short.

% Applications (zeolites generally)

\subsubsection{Applications}
\label{introduction:zeolite_applications}

The possibilities for commercial application of zeolites largely depends on their Si\stroke{}Al ratio, the (dis)order in the Si\stroke{}Al distribution, the nature and positioning of the extra-framework cations within the pores, and of course the dimensions of the zeolitic pores and channels.  Zeolites' inherent tunability --- in terms of acidity, reactivity, and stability, for example --- capacity to undergo ion-exchange, and ability to withstand certain extremes in experimental conditions, are the most valuable properties for industrial applications.

Zeolites not only provide greater overall surface areas, over which reactions may take place, than their denser solid-state analogues, making them particularly useful as adsorbents in catalysis, but they also enable reactions to take place selectively within their channels and pores, depending on their respective dimensions and steric compatibility.~\cite{Kaeding1981_Part2,Young1982_Part3,Kaeding1984_Part4,Kaeding1985_Part5,Kaeding1988_Part6,Kaeding1989_Part7,Chen1996}  Synthetic zeolites are also tunable to an extent, in terms of their design and creation, since they are ``grown'' around organic templates, which themselves can be tailored and synthesised for the purpose.

The aluminium-rich subset of zeolites, such as zeolite A,~\cite{REF} with their higher proportions of charge-compensating sites, are useful as sorbents~\cite{Vjunov2014} and as catalysts for specific kinds of reaction.~\cite{Machado1999}

The more silicon-rich distributions~\cite{Martens1987}
% such as ZSM-5,~\cite{Burger2000}
lend themselves well to catalytic applications, as their distinct ion-exchange sites can be used more selectively, in contrast to the more generalised ion-exchange properties of aluminium-rich zeolites; the stability provided by a high Si\stroke{}Al ratio enables such zeolites to withstand higher temperatures, such as those often required in catalytic processes.~\cite[p.~2]{ZeoliteScienceAndPractice}

The uniformity of zeolitic pores allows for their use in shape-selective reactions; the pores and channels of the zeolite impose a steric constraint on the reactions taking place within them.  They can, therefore, guide such reactions towards better yields of the desired product.~\cite[pp.~327-370]{CatalysisAndZeolites_BOOK}\textsuperscript{;}\cite{Schenk2001}  Zeolites are well-known commercially for their use in the petrochemical and oil refinement industries, for example in crude oil ``cracking'' processes,~\cite{Yang2003_BOOK} the premise of which involves the ``splitting'' of long-chained hydrocarbons into shorter-chained ones, and so within the zeolite only those shorter than a specific length --- determined by the diameter of the zeolitic pores and channels --- are free to pass through, whereas those exceeding said length, before or after reacting, are either retained in the pores indefinitely or until they have reacted and ``split'' further.  Similar reasoning enables zeolites' use in gas-separation reactions,~\cite{Ackley2003} where the gaseous components to be separated are clearly distinguishable by size.

They have also found use in laundry detergents~\cite{Cutler1987,Sekhon2004} --- undergoing ion-exchange to remove the ``hard'' Group 2 cations from the washing water and replace them with ``softer'' Group 1 cations, thus reducing the ``dulling'' effect of the harder cations on the laundered fabrics --- and in horticulture and agriculture, due to their soil-conditioning potential, as they can aid in the retention of nitrogen, potassium, and water in the soil, reducing the need for soil ``refreshment'',~\cite{Valente1982} and for the treatment of manure to make the best use of its ammonia,~\cite{Ghan2005} enabling it to be used more efficiently as fertiliser.~\cite{Nguyen1998}

One important area of interest for this study specifically is that of zeolitic storage applications.  Zeolites are already in widespread use in wastewater treatment,~\cite{Nguyen1998} as they can effectively neutralise the harmful components of the wastewater by trapping them within their pores; they have also been utilised as nuclear waste storage media,~\cite{Mercer1978,Dyer1984} which is particularly relevant given current industrial demands for more sustainable alternatives to fossil fuels.  This particular application makes use of those zeolites which expand under pressure, enabling the nuclear waste to be inserted into their pores, and around which the zeolite contracts when the pressure is removed.  Some zeolites, such as laumontite, have found use as accelerants for the cement-hardening process,~\cite{Ono1982} which further augments their particular nuclear-waste-storage potential, since cement could therefore be used as an additional containment layer for toxic and\stroke{}or radioactive materials within such zeolites' pores.

\subsection{The natrolite family}
\label{introduction:natrolite_family}

The natrolite family will be the main focus of the first stages of this investigation.  Natrolite, scolecite, and mesolite all share the NAT topology, with a Si\stroke{}Al ratio of 1.5, and are of particular interest because they have ordered Al distributions within the silicate frameworks, making them comparatively easy to model and analyse.  They also undergo interesting high-pressure- and high-temperature-induced phenomena: pressure-induced hydration~\cite{Lee2001,Lee2002_PIExpansion,Lee2002_K-GaSi-NAT,Lee2006,Lee2010,Lee2013,Seoung2013,Seoung2015} and negative thermal expansion,~\cite{Lee2001} respectively, which are important properties requiring further study, since they have proven useful in storage-based applications.

% CF: Other zeolites that undergo PIH include zeolite type A~\cite{Secco2000}, laumontite~\cite{Fridriksson2003,White2004,Rashchenko2012}, gismondine~\cite{Lee2008_K-GaSi-GIS,Orri2008}, FER-type zeolites~\cite{Arletti2014}, MFI-type zeolites~\cite{Vezzalini2014}, boggsite~\cite{Arletti2010}, thomsonite~\cite{Likhacheva2007}, mordenite~\cite{Arletti2015}
% General/review papers including NTE include~\cite{Lightfoot2001,Baur2003,Neuhoff2004,Liu2011,Gatta2014}

Natrolite contains sodium as its extra-framework cation, while scolecite contains half the number of divalent calcium instead,
% thus having more empty space within its pores than natrolite, but also has a distorted framework structure as a result of the reduced number of cations,
and mesolite is currently considered to be composed of layers, containing sodium and calcium alternately.

% Current uses

Natrolite is currently used in the water-purification and chemical-filtering industries, capitalising on its ion-exchange properties.~\cite{Barrer1959,Margeta2013}  Scolecite is particularly useful in wastewater decontamination for the same reason.~\cite{DalBosco2005,Wang2010}  Mesolite has been utilised as storage for tritiated water,~\cite{Faghihian1998} and is thus also promising as a storage medium for radioactive waste.~\cite{Sharma2008}  It is the development of possibilities in this area that this investigation is well-placed to consider from a computational standpoint, by focusing on modelling the fully-hydrated natrolite family as accurately as possible, particularly those extreme conditions that are hard to produce experimentally, and which are the most pertinent for safety-related studies of these applications.

\subsection{Research importance}
\label{introduction:research_importance}

\hide{\subsubsection{Effects of temperature and pressure}
\label{introduction:temperature_pressure_importance}

The nature of zeolites' three-dimensional open frameworks widen the scope for temperature- and pressure-induced structural distortions.  Phenomena such as negative thermal expansion~\cite{Lightfoot2001} and pressure-induced hydration~\cite{Lee2001} are fairly common amongst microporous materials.~\cite{Lightfoot2001}

A computational investigation --- or indeed several investigations --- into the temperature- and pressure-induced effects on various zeolitic properties will aid the expansion of the existing body of knowledge on the subject, and can be performed without the requirement to physically reproduce the required conditions.}

\subsubsection{Natrolites}
\label{introduction:natrolites_importance}

The natrolite family of zeolites are comparatively dense for microporous materials.~\cite{Lee2002_PIExpansion,Baur2003}  They undergo pressure- and temperature-induced structural changes,~\cite{Lee2001} which may provide more insight into the wider potential for their usage industrially.

They are particularly applicable for the future development of industrial usage as nuclear waste storage materials,~\cite{Kazemian2001} and are thus of interest to this investigation.  Details of zeolite-specific behaviour in these high-pressure and high-temperature conditions, or indeed the formulation of models to better represent zeolites under such conditions, will increase the accuracy of predictions about their potential applications, and indeed may enable the ``filtering out'' of zeolites with unsuitable structures and\stroke{}or properties, without the need for physical testing at such an early stage.

\oiface{But they are ideal because they are simple --- small unit cell, few T sites, ordered, all EFC and water molecules are well-characterised.}

\subsection{Research questions}
\label{introduction:research_questions}

\subsubsection{Modelling zeolitic temperature and pressure effects}
\label{introduction:modelling_temperature_pressure_effects}

Since the conditions in which a zeolite is formed are instrumental in the resultant mineral's structure, stability, and properties, it is unsurprising that changes in these conditions will alter the chemistry of the zeolite in question.  However, it is not always viable to test the effect of changes in these conditions on a zeolite experimentally, particularly when the extremes of pressure and temperature that may be important for investigating potential applications of the zeolite are either costly or dangerous to recreate.

Computational simulations can offer a much easier route to exploration of these environmental effects without the practical limitations of more traditional techniques, but they are not without their own limits.  As would be expected, as the size of a system being simulated increases, so does the runtime of the simulation and the computational power required to run it; likewise, the more accurate the model used to describe the system, the more those same limitations apply.

Much of computational research is dependent on reaching this optimal balance between the representative model's accuracy and computational expense, which can often rely on a trial-and-error approach to find; the main modelling-related question in this investigation asks where this balance lies in regards to zeolites, and how best to achieve it.

Therefore, this investigation aims to answer the question: how well can zeolites, under extremes in thermodynamic conditions, such as temperature and pressure, be modelled?  And which theoretical description is necessary?  Can \oiface{[something]} potential methods perform sufficiently, or is a quantum mechanical approach required?

A logical question, specific to the natrolite family, to follow this would be: which models --- and their associated parameters --- have the least complexity or computational intensity, but retain enough accuracy to fully reproduce experimental observations for the natrolite family of zeolites?

% Hydration states
\subsubsection{Zeolitic hydration states}
\label{introduction:hydration_states_importance}

Geologically, the hydration states of zeolites are of particular importance because zeolites are hydrated in their natural form; thus, modelling them in dehydrated
% [anhydrous?]
form is of little consequence when considering applications, when water is essential to their formation and properties.

Moreover, the presence, and amount, of water in zeolitic pores has been shown to influence the distribution of aluminium in the silicate frameworks,~\cite{Almora-Barrios2001} and also the positioning of the charge-compensating cations, in relation to the incorporated water molecules, which provide both electrostatic and steric shielding between the cations and frameworks.

This investigation is particularly focused on the effects of pressure and temperature on the hydration states of zeolites, since the current specialist disposal sites using zeolites as nuclear waste storage are subject to environmental changes; the security of the nuclear waste within the storage materials and the safety of those handling them are of paramount importance, and so this area requires further study, in order to prevent the possibility of environmental contamination from the unanticipated structural changes in nuclear waste storage materials.  Therefore, a further question to be investigated is whether the long-term stability of zeolites is adversely affected by consistently high pressures and temperatures, and/or the frequent changing in these conditions.

% Given the typical environmental conditions found in nuclear waste storage depositories, at what point does changing these conditions cause the safety of the nuclear waste storage materials to be compromised?

% Natrolite and scolecite generally vs. experiment

% \oiface{
\subsubsection{Dehydration of natrolite and scolecite}
\label{introduction:nat_sco_dehydration}

% ... so we can now say generally that it probably will work well, but not well enough, to do mesolite.

Given the current ability of potential-based models to relatively accurately predict the dehydration of natrolite and scolecite, a further question of importance would be whether the same models could also represent mesolite similarly well.  The increased complexity of mesolite's observed dehydration process would suggest they would not, in which case, it would be useful to work out the least-complex model that does accurately predict it.
% }

% Mesolite thermodynamics

\subsubsection{The thermodynamics of dehydration of mesolite}
\label{introduction:mesolite_thermodynamics}

Experimentally, it has been found that mesolite dehydrates by losing the calcium-coordinated water molecules first, but this is reversed in the computational predictions for the dehydration of mesolite using interatomic-potential models,~\cite{Gibbs2004} as compared to experimental findings.~\cite{Stahl1994_XRD_Mes}  The strength of binding between calcium and water and sodium and water, respectively, have evidently not been sufficiently accurately constructed, given the same parameters as natrolite and scolecite.

A further aim of this investigation would be to ascertain whether the issue causing this is with the type of model being used, the parameters within the model, or the currently-accepted structure --- deduced from crystallographic data --- on which the input file coordinates are based.  This will ideally be tested first by comparison between the optimised output from the interatomic-potential models used with the General Utility Lattice Program~\cite{Gale1997,Gale2003,Gale2005} (GULP) and the output of ``intermediate'' tests, using ``hybrid'' models, which will take the atomic coordinates from the generated GULP~\cite{Gale2003} input files and run them through CP2K~\cite{CP2K,Hutter2014} with more complex electronic-structure models.

\subsection{Hypothesis}
\label{hypothesis}

% Follow the low-energy pathway of dehydration for natrolite, scolecite, and mesolite. Compare against previous investigation's results for the former two zeolites, and compare against experimental findings (XRD, TGA).

The aim of this investigation is to find out, if the lowest-energy pathways of dehydration for natrolite and scolecite are followed computationally, using existing interatomic potential models as have been used in previous studies,
% in GULP~\cite{Gale2003}
whether or not the current potential parameters are sufficiently robust to reproduce experimental findings. % A secondary aim would be to follow the high-energy pathways of dehydration for the same, with the intention to compare this pathway against the low-energy one, in terms of whether or not the thermodynamic properties of interest have better agreement with experiment in the low-energy pathway, as would be expected.

% Follow low- and high-energy pathways for mesolite, to see whether either match experimental observations.  This is the New Work, extending that done previously.  The hope would be that they match, but probably do not (based on existing computational data), so the aim in that case would be to pinpoint the cause of any disagreements and propose (and ideally also test) some solutions.

An additional aim would be to compare both high- and low-energy pathways of dehydration for mesolite with the observed experimental water-removal ordering, extending previous work carried out on this member of the natrolite family.  If the low-energy pathway is not in agreement with the experimental data, pinpointing the cause of the divergence --- and proposing some testable solutions --- would be an important goal for future studies.

Since the currently-accepted structure for mesolite is composed of alternating natrolite-like and scolecite-like layers, it follows that mesolite would be predicted to display both natrolite-like and scolecite-like dehydration behaviour, as described in \textbf{Section~\ref{litrev:expt}}, since the prediction of more complex behaviour such as amorphisation --- as is observed at a later dehydration stage experimentally --- are not within the scope of present computational techniques.  However, the current interatomic potential model, which has been used quite successfully to describe natrolite and scolecite --- with the exception of their respective volume changes during the process of dehydration being less-strongly predicted by simulation --- does not describe mesolite particularly well.  Such models suggest that the low-energy pathway for dehydration is followed via the removal of those water molecules coordinated to sodium cations first, in direct contrast to experimental observations that water molecules coordinated to the calcium cations would be removed first.  There are arguments for both possibilities; sodium's lesser charge compared to calcium intuitively suggests it would coordinate less strongly to its surrounding water molecules, whereas calcium is larger, and its charge distribution is therefore more diffuse than that of sodium, and it ``shares'' its charge with more (seven~\cite{Zavitsas2005}) water molecules than does sodium (which coordinates to six water molecules~\cite{Schulz1986}), so it may be that this diffusivity, coupled with a greater ``demand'' from the surrounding water molecules, outweighs the simple quantitative charge difference between it and sodium.

Discrepancies may likely be explained by insufficiently robust potential parameters guiding the extra-framework cation (EFC)-water interactions, but it remains to be seen whether any improvement on the existing parameters can be made.  Therefore, the current primary question is whether or not an interatomic potential model can be fitted so as to sufficiently reproduce experiment, based on the starting model, but optimised so as to enable mesolite's dehydration pathway to be predicted more accurately without sacrificing the accuracy of any other calculated properties.  If not, the potential application of higher-level, quantum-mechanics-based methods will need to be explored in greater detail.

% What happens when you swap stuff around all over the place in mesolite and then run through GULP again?  \emph{*Cue dramatic music*} {\ldots} watch this spaaaace {\ldots}

\oiface{}

Computational expense is a concern for the running of simulations on systems of this size.  Using higher-level models, such as density functional theory, for such systems is significantly more expensive than simulations run using interatomic potential models.  Ideally, the speed and resources required for higher-level calculations to run could be reduced by pre-optimising their structures using interatomic potential models, and using those structures as input for more complex models, and further optimising those.  Given the signficant trade-off required between accuracy and computational cost at higher levels of theory, the question remains as to whether such complex models are needed, or indeed worth the additional cost for the gain in accuracy they can provide.

% Open question as well is what level of DFT needed?

\oiface{}

\newpage
