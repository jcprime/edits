#include <stdio.h>
#include "structures.h"
#include "global_values.h"
#include "header.h"

/* routine to give the vector square of the separation between two atoms */
/* with minimum image if the pbc flag is set                             */

// Work out minimum image vector for periodic systems
void min_image( double *x, double *y, double *z);

// Go from fractional to Cartesian coordinates for a single 3-component vector
void fract_to_cart( double *cart_x, double *cart_y, double *cart_z,
                    double  frac_a, double  frac_b, double  frac_c,
                    double *p_latt_vec );

double atom_separation_squared(atom *p_A, atom *p_B, int pbc)
{
    double dx, dy, dz, r2;
    double temp_ax, temp_bx;
    double temp_ay, temp_by;
    double temp_az, temp_bz;

    /** convert to cart first **/
    fract_to_cart(  &temp_ax, &temp_ay, &temp_az,
                    p_A->x, p_A->y, p_A->z, &latt_vec[0]);
    fract_to_cart(  &temp_bx, &temp_by, &temp_bz,
                    p_B->x, p_B->y, p_B->z, &latt_vec[0]);
    dx = temp_ax - temp_bx;
    dy = temp_ay - temp_by;
    dz = temp_az - temp_bz;

/***************************************************************************/
/******* if this is a periodic pore use the nearest image convention *******/
/***************************************************************************/

    if (pbc)
    {
        min_image(&dx, &dy, &dz);
    }

    r2 = dx*dx + dy*dy + dz*dz;

    return r2;
}
