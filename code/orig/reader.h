enum { PRIME_DIRECTIVE= 1, SECONDARY_DIRECTIVE, TERTIARY_DIRECTIVE, REMAINING_DIRECTIVE };

enum {TITLE = 1,
        DEHYDRATE,
        OXYGEN,
        HYDROGEN};

enum {ATTEMPTS = 101,
        LABEL};


enum {MAYBE = 201,
        ALL, ON3, OFF3};


enum {DEFAULT = 666};
enum {UNIT = 998};
enum {PARSE = 1001};

enum {BLANK_DIRECT = 999};

/*** note two defintions of non_bonded as nonb and non_ 21.10.96 DWL ***/
#define FIRST_DIRECTIVE_LIST \
    "titl", TITLE,\
    "dehy", DEHYDRATE,\
        "oxyg", OXYGEN,  \
        "hydr", HYDROGEN,  \
    "",BLANK_DIRECT

#define SECOND_DIRECTIVE_LIST \
	"",BLANK_DIRECT

/*** NOTE replacing of yes/no wiht on/off which solves probs  21.10.96 DWL ***/
#define THIRD_DIRECTIVE_LIST \
       "ma", MAYBE,\
           "al", ALL,\
           "on", ON3, \
           "of", OFF3, \
	   "", BLANK_DIRECT

#define NULL_DIRECTIVE_LIST \
       "an", UNIT,             "(a", UNIT,           \
       "(c", UNIT,             "k ", UNIT,           "(k",  UNIT, \
       "#", PARSE, "", BLANK_DIRECT

typedef struct
{
  char *directive;
  int token_index;
}list;

