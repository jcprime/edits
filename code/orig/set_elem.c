/*************************************************************************/
/* set_elem                                                              */
/* Sets up:                                                              */
/*  - element name                                                       */
/*  - vdW radius                                                         */
/* from GULP input                                                       */
/* Mode of calculation determined by input file                          */
/*                                                                       */
/* Started DWL 10/2/98                                                   */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "maxima.h"
#include "global_values.h"
#include "structures.h"
#include "data.h"

void set_elem(atom *p_molecule, int num_atoms)
{
    #include "header.h"
    atom *p_atom;

    int i, j;
    BOOLEAN found_atom_flag;

/****** Loop through the atom structure,      **********/
/****** calculate the non-bonding parameters, **********/
/****** and assign van der Waals radii        **********/

    /****** Loop through all elements ******************/

    for (i = 0; i <= num_atoms; i++)
	{
    	found_atom_flag = FALSE;
    	p_atom = p_molecule + i;

        // Note: NUM_ELEMENTS is defined as 104 in maxima.h
    	for (j = 0; j < NUM_ELEMENTS; j++)
		{
            // Compare the first two characters of p_atom->label against the first two characters of period_table[j].elem
            if ((strncmp(p_atom->label, period_table[j].elem, 2)) == 0)
			{
                // Need to use strcpy for elem but not the others because it's a string and they're all numbers
                strcpy(p_atom->elem, period_table[j].elem);
    			p_atom->vdw = period_table[j].vdw;
    			found_atom_flag = TRUE;
    			break;
			}
		}
    	/* if we haven't found the right element then re-check for 1 letter */
    	if (found_atom_flag == FALSE)
        {
            for (j = 0; j < NUM_ELEMENTS; j++)
            {
                if ((strncmp(p_atom->label, period_table[j].elem, 1)) == 0)
                {
                    strcpy(p_atom->elem, period_table[j].elem);
                    p_atom->vdw = period_table[j].vdw;
                    found_atom_flag = TRUE;
                    break;
                }
            }
        }
    	/* if we haven't found the right element then Serious Error */
    	if (found_atom_flag == FALSE)
		{
    		printf("ILLEGAL Element\n");
    		printf("Atom number %i, element given as %s\n",
                    i, p_atom->elem);
    		printf("ILLEGAL Element\n");

    		fprintf(output_fp,"Atom number %i, element given as %s\n",
                    i, p_atom->elem);
    		fprintf(output_fp,"Aborting...\n");
                    fflush(output_fp);
                    fflush(stdout);
    		exit(1);
		}
	}
    return;
}
