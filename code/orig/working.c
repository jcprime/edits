/****************************************************************/
/* MC_subs:  Utility to generate subsituted silica polymorphs   */
/*           for running in  Gulp                               */
/* DWL 05/02/98                                                 */
/****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "maxima.h"
#include "global_values.h"
#include "structures.h"

#define MAIN 0
#include "header.h"
#include "data.h"
#include "ewald.h"
#include "own_maths.h"
#undef MAIN

#define MAXATOMS 5000
#define BUFFER    256
#define TRUE        1
#define FALSE       0

simple_atom h_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);
void save_config(FILE *file_fp, int config, int num_replace_atoms, 
                 double final_energy,
                 int *p_replaceable_index,int *p_replaceable_done);


void generate_neighbours( atom *p_molecule, int num_atoms,
                          atom_number *p_types, int *p_num_types,
                          int use_pbc);
void print_neighbours( atom *p_molecule, int num_atoms, FILE *fp);

void print_molecule(atom *p_molecule, int num_atoms, FILE *output_fp);

void set_elem(atom *p_molecule, int num_atoms);
void steric_setup(atom *p_molecule, int num_atoms);
double real_random(int done);
void index_sort(int n, double *p_arrin  ,int *p_indx);
void save_xtlfile(FILE *file_fp, atom *p_mol, 
                   int num_atoms, char *p_spacegroup);
                   
void save_gulpfile(FILE *file_fp, atom *p_mol, 
                   int num_atoms, char *p_spacegroup,
                   char *p_xtlname, char *p_pots_file);

void main(argc, argv)
	int   argc;
	char  **argv;
{
        int found_shell, got_one;
        int first_shell;
        double core_shell_sep;
	int i, j;
	int iloop;
        int ivec;
        int max_trials, this_trial;
        int  subs_done;
	int num_atoms;
        int lowenstein_break;
	int new_num_atoms;
        int num_core_atoms;
        int this_neigh, that_neigh;
	int ineigh;
	int jneigh;
	int num_mol_types;
	char dummy[81];
	char buffer[81];
	atom mol[1000];
	atom orig_mol[1000];
        simple_atom cation_pos, T1, T2, bridgeO;
	int replaceable_index[300];
	int replaceable_done[300];
	atom_number mol_types[10];
        int random_seed;
	FILE *infp;
	FILE *gulpfp;
	FILE *dumpfp;
	FILE *gulpoutputfp;
	FILE *coordinate_fp;
	FILE *config_fp;
	char *fullstop;
	char gulpfile_root[81];
	char dumpfile[81];
	char config_file[81];
	char pot_file[81];
        char spacegroup[20];
        char append_pots_command[256];
        int final_energy_flag;
        int initial_energy_flag;
        int replace_this;
        int swap;
        int got_it;
        int found_replacable;
        int num_replace_atoms;
        int in_list;
        int hot_list_index[100];
        int hot_list_sites[100][30];
        double hot_list_energy[100];
        double final_energy;
	char this_label[4];
	int num_si, num_h, num_oh, num_o, num_other;
        double a,b,c, alpha, beta, gamma;
	
    if (argc != 2) 
		{
		fprintf(stderr, "Usage: %s  file\n", argv[0]);
          	exit(1);
    	        }
	else
		{
		if ((infp = fopen(argv[1], "r")) == NULL) 
			{
			fprintf(stderr, "Unable to open file %s for input.\n", argv[1]);
			exit(1);
			}
		}
	

        strcpy(output_file, argv[1]);
        strcpy(config_file, argv[1]);
       strcat(output_file, OUTPUT_EXTENSION);
       strcat(config_file, CONFIG_EXTENSION);
        if ((output_fp = fopen(output_file, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", 
                            output_file);
            exit(1);
            }
        if ((config_fp = fopen(config_file, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for configurations\n",
                            config_file);
            exit(1);
            }


        /*defaults and error catchers*/
        max_subs = -1;
        ratio = -1;
        gulp_max_cycles = 100;
        gulp_time_limit = 6000;

        /* read in command from file */
        reader(argv[1]);
        /* read in commands from file argv[1] */
        /* print out input paramaters */
        fprintf(output_fp,"MC_subs running...\n");
        fprintf(output_fp,"Input:\n");
        fprintf(output_fp,"TITLE:%s\n", title);
        fprintf(output_fp,"COORD: %s\n", coordinate_file);
        fprintf(output_fp,"POTS : %s\n", pots_file);
        fprintf(output_fp,"GULP : %s\n", gulpfile);
        fprintf(output_fp,"CYCLE: %i\n", max_cycles);
        fprintf(output_fp,"PEEK : %i\n", peek_freq);
        fprintf(output_fp,"KEEP : %i\n", keep);
        fprintf(output_fp,"CATION : %s %f\n", cation, cation_charge);
        fprintf(output_fp,"REPLACE : %s %f\n", replace_ele, replace_charge);
        fprintf(output_fp,"INSERT : %s %f\n", insert_ele, insert_charge);
        if (max_subs > 0) fprintf(output_fp,"MAX_SUBS: %d\n", max_subs);
        else fprintf(output_fp,"RATIO: %f\n", ratio);
        fprintf(output_fp,"RANDOM: %d\n", randomisation);

	/****** read in unit cell file ******/
        if ((coordinate_fp = fopen(coordinate_file, "r")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for input.\n", 
                             coordinate_file);
            exit(1);
            }

	printf("Reading coordinate file %s....\n", coordinate_file);


	/** expecting cell line, followed by space line followed by frac **/

	fgets(dummy,BUFFER,coordinate_fp);
	if (strstr(dummy, CELL) == NULL)
		{
		printf("Expecting cell paramters. Exiting...\n");
		exit(0);
		}
    	fgets(dummy,BUFFER,coordinate_fp);
	sscanf(dummy,"%lf%lf%lf%lf%lf%lf", 
			&abc[0],
                     &abc[1], &abc[2], &abc[3], &abc[4],
                     &abc[5]);

printf("cell: ");
for (iloop = 0; iloop < 6; iloop++)
              {
                printf("%10.6f   ",abc[iloop]);
              }
printf("\n");
        pbc = 1; /*****FUDGE!***/	
        fgets(dummy,BUFFER,coordinate_fp);
        if (strstr(dummy, SPACE) == NULL)
                {
                printf("Expecting spacegroup info. Exiting...\n");
                exit(0);
                }
        sscanf(dummy,"%*s%s", spacegroup);

        fgets(dummy,BUFFER,coordinate_fp);
        if (strstr(dummy, FRAC) == NULL)
                {
                printf("Expecting fractional coord. Exiting...\n");
                exit(0);
                }

	 i = 0;
         while ((fgets(buffer,BUFFER,coordinate_fp) != NULL))
		{
                sscanf(buffer,"%s%s%lf%lf%lf%lf", &orig_mol[i].label,
                                &orig_mol[i].type,
                                &orig_mol[i].x, &orig_mol[i].y, &orig_mol[i].z,
                                &orig_mol[i].charge);

		/***init neighbour info ***/
		orig_mol[i].num_neigh=0;
		for (ineigh=0;ineigh<4;ineigh++) orig_mol[i].neighb[ineigh]=-1;


		i++;
		}
	num_atoms = i-1;		

	fclose(coordinate_fp);

/*****************************************************************/
/******* sort out lattice vectors if required ********************/
/*****************************************************************/

  if (pbc)
    {
       cart_latt_vecs( &abc[0], &latt_vec[0], &real_latt_sizes[0],
                        &recip_latt_vec[0],
                       &recip_latt_sizes[0], &cell_volume, num_atoms);



/**** THIS NOW NEEDS TO GO TO THE OUTPUT FILE ******/

       ivec=0;
       for (iloop = 0; iloop < 9; iloop++)
         {
            /*printf("%10.6f  ", latt_vec[iloop]);*/
            if (iloop == 2 || iloop == 5 || iloop == 8)
                {
                /*printf ("  :  size = %10.6f\n",real_latt_sizes[ivec]);*/
                ivec++;
                }
         }

       /*printf ("\nReciprocal lattice vectors :\n\n");*/

       ivec=0;
       for (iloop = 0; iloop < 9; iloop++)
         {
            /*printf("%10.6f  ", recip_latt_vec[iloop]);*/
            if (iloop == 2 || iloop == 5 || iloop == 8)
               {
                  /*printf ("  :  size = %10.6f\n",recip_latt_sizes[ivec]);*/
                  ivec++;
               }
         }
        /*printf ("\nCell Volume = %10.6f cubic Angstroms\n\n",cell_volume);*/
        /*printf("Maxima for ewald sum: real space= %10.6f, recip space = %10.6f\n\n", real_sum_max,recip_sum_max);*/


    }

/******************************************************************/
/***** Assign vdw radii to the pore and to the fragment library ***/
/******************************************************************/

  set_elem(&orig_mol[0],num_atoms);

  steric_setup(&orig_mol[0],num_atoms);
/*
for (i=0;i<=num_atoms;i++)
    {
printf("Atom %d: %s %s %s %lf %lf %lf %lf %lf\n", i, orig_mol[i].label,
               orig_orig_mol[i].elem,
                                orig_mol[i].type,
                                orig_mol[i].x, orig_mol[i].y, orig_mol[i].z,
                                orig_mol[i].charge,orig_ mol[i].vdw);

    }
*/
	
/************ ONLY DO NEIGHBOURS ON CORES *******/
/**assume input is T core, O core, O shel**/
/*calc number of cores*/
        num_core_atoms=0;
        for (i=0;i<=num_atoms;i++)
           {
           if (strstr(orig_mol[i].type, CORE) != NULL)
            num_core_atoms++;
           }
        num_core_atoms--;
/*find first shells*/
first_shell = num_core_atoms+1;
	/****** generate neigbours *****/
	generate_neighbours(&orig_mol[0], num_core_atoms, &mol_types[0],
						&num_mol_types, TRUE);
	print_neighbours(&orig_mol[0], num_core_atoms,stdout);
	printf("Finshed reading....\n");

/**** initialise gulp input file ****/
        printf("gulpfile %s\n", gulpfile);
        if ((fullstop = strchr(gulpfile,'.')) !=NULL) *fullstop = '\0';

        strcpy(gulpfile_out, gulpfile);
        strcat(gulpfile, "_temp.gin");
        strcat(gulpfile_out, "_temp.gout");
        printf("Will use Gulp file: %s\n", gulpfile);
        printf("Will read output  : %s\n", gulpfile_out);


/***calcualte max-sub = no.Si/ratio ****/
/***determine how many "replace" atoms there are***/
num_replace_atoms=0;
for (i=0;i<=num_core_atoms;i++)
    {
     if (strcmp(replace_ele, orig_mol[i].elem) == 0)
          {
           replaceable_index[num_replace_atoms] = i;
           replaceable_done[num_replace_atoms] = FALSE;
           printf("Atom %d %s is replaceable\n", i, orig_mol[i].label);
           num_replace_atoms++;
          }
        
    }
num_replace_atoms--;


/*****initialise hot list******/
/*******
for (i=0;i<keep;i++)
    {
     hot_list_index[i] = i;
     hot_list_energy[i] = 9999999-i;

     for (j=0;j<=num_replace_atoms;j++) hot_list_sites[i][j] = -1;
    }
for (i=0;i<keep;i++)
    {
     printf("HOT %d %f\n", hot_list_index[i], hot_list_energy[i]);
    }
index_sort(keep, (&hot_list_energy[0])-1, (&hot_list_index[0])-1);
for (i=0;i<keep;i++)
    {
     printf("HOT %d %f\n", hot_list_index[i], hot_list_energy[i]);
    }
*******/

/*****determin number to be substituted********/
        if (ratio > 0)
           {
           /***ratio given in file****/
           max_subs = num_replace_atoms/ratio;
            printf("Target ratio given = %f. Equivalent to inserting %d atoms\n",
                      ratio, max_subs);
           }
         else
           {
            printf("Target substitution given = %d\n", max_subs);
           }



/***************************WRITE HEADER FOR config.s file*********/
fprintf(config_fp, "#CONFIGURATION file\n");
fprintf(config_fp, "cell %s\n", coordinate_file);
fprintf(config_fp, "replace %s  %6.3f\n", replace_ele, replace_charge);
fprintf(config_fp, "insert %s  %6.3f\n", 
                    insert_ele, insert_charge);
fprintf(config_fp, "subs %d\n", max_subs);
fprintf(config_fp, "lowenstein ");
if (lowenstein) fprintf(config_fp, " on\n");
else fprintf(config_fp, " off\n");
fprintf(config_fp, "cation %s\n", cation);
fprintf(config_fp, "pots %s\n", pots_file);
fprintf(config_fp, "keep %d\n", keep);
fprintf(config_fp, "%s\n", CONFIGURATIONS);
/***************************LOOP: no configs to try ***********************/
/***************************LOOP: no configs to try ***********************/
/***************************LOOP: no configs to try ***********************/

for (this_trial = 0; this_trial <= max_cycles; this_trial++)
    {
        /****re-initialise atom list****/
        for (i=0;i<=num_atoms;i++)
         {
          strcpy(mol[i].label, orig_mol[i].label);
          strcpy(mol[i].type, orig_mol[i].type);
          mol[i].x =  orig_mol[i].x;
          mol[i].y = orig_mol[i].y;
          mol[i].z = orig_mol[i].z;
          strcpy(mol[i].elem ,orig_mol[i].elem);
          mol[i].charge = orig_mol[i].charge;
          mol[i].num_neigh= orig_mol[i].num_neigh;
          for (j=0;j< 4;j++) mol[i].neighb[j] = orig_mol[i].neighb[j];
          

         }
        /****re-initialise replaceable_done****/
        for (i=0;i<=num_replace_atoms;i++)
         {
         replaceable_done[i] = 0;
         }
        printf("there are %d replacable atoms\n", num_replace_atoms);

/******
        for (i=0;i<=num_replace_atoms;i++)
            {
             printf("Index %d %d Atom %s status = %d\n", 
                      i, replaceable_index[i], 
                      mol[replaceable_index[i]].label,
                              replaceable_done[i]);
            }
*******/


   	 /******LOOP: for each substituent required *****/
        subs_done = 0;
        new_num_atoms = num_atoms;
        while (subs_done < max_subs)
            {
             /***generate random substituent****/
             replace_this = real_random(1)* num_replace_atoms;
             /*find 'replace_this'th replaceable atom*/
             /*is it already replaced*/
             /* no - then ...*/
             if (replaceable_done[replace_this] == FALSE)
               {
                /***is Lowensteinian behaviour okay - optioN? ****/
                if (lowenstein == TRUE)
                   {
                    /**loop over all neighbours of replace_this and see if**/
                    /**any of their neighbours are "done"                 **/
                    lowenstein_break = FALSE;
                    for (ineigh = 0; 
                         ineigh<mol[replaceable_index[replace_this]].num_neigh; 
                         ineigh++)
                        {
                         this_neigh 
                           =mol[replaceable_index[replace_this]].neighb[ineigh];
                         for (jneigh = 0; jneigh<mol[this_neigh].num_neigh; jneigh++)
                           {
                            that_neigh = mol[this_neigh].neighb[jneigh];
                            if (strstr(mol[that_neigh].label, insert_ele) !=0)
                               {
                                lowenstein_break = TRUE;
                                break;
                               }
                           }
                        }
                   }

                if ((lowenstein == FALSE) || (lowenstein_break == FALSE))
                   {
                    replaceable_done[replace_this] =TRUE;
                    swap = replaceable_index[replace_this];
                    strcpy(mol[swap].label, insert_ele);
                    strcpy(mol[swap].elem, insert_ele);
                    mol[swap].charge = insert_charge;
                    subs_done++;
                   }
                 else
                   {
                   }
                
               }
             else 
               {
               }
            }
    	/***endLOOP****/

        /****CHARGE COMPENSATE*****/
        if (strstr(cation, SMEAR) != NULL)
           { 
           /***spread charge on other T sites ***/
           cation_charge= -subs_done*(replace_charge-insert_charge);
           cation_charge= cation_charge/(num_replace_atoms-subs_done+1);
           for (i=0;i<=num_replace_atoms;i++)
             {
              if (replaceable_done[i] != TRUE)
                 {
                 swap = replaceable_index[i];
                 mol[swap].charge = mol[swap].charge - cation_charge;
                 }
             }
           }
        else if (strstr(cation, OH) != NULL)
           {
            printf("Doing OH insertion\n");
            /**do OH Subs***/
            /***LOOP ALL REPLACED ATOMS****/
            for (i=0;i<=num_replace_atoms;i++)
             {
              if (replaceable_done[i] == TRUE)
               {
                /***select random O****/
                printf("finding O for %s %d\n", mol[replaceable_index[i]].label,
                                                replaceable_index[i]);
                got_one = FALSE;
                while (got_one != TRUE)
                 {
                 replace_this=
                     real_random(1)*mol[replaceable_index[i]].num_neigh;
                 this_neigh = mol[replaceable_index[i]].neighb[replace_this];
                 printf("checking %s\n", mol[this_neigh].label);
                 /***check no OH ****/
                 if (strstr(mol[this_neigh].label, OH_SPECIES) == 0) 
                    got_one= TRUE; 
                 }
                 printf("selected %s as neighbour of %s %d\n", 
                      mol[this_neigh].label, mol[replaceable_index[i]].label,
                      replaceable_index[i]);
                 /*find it's other T site neighbour*/
                 got_one = FALSE;
                 while (got_one != TRUE)
                  {
                   for (j=0;j<=mol[this_neigh].num_neigh;j++)
                    {
                     that_neigh = mol[this_neigh].neighb[j];
                     printf("is %s %d same as %s %d\n", mol[that_neigh].label,
                                           that_neigh,
                                           mol[replaceable_index[i]].label,
                                           replaceable_index[i]);
                     if (that_neigh != replaceable_index[i])
                      {
                       got_one = TRUE;
                       printf("no\n");
                       break;
                      }
                    }
                  if (got_one != TRUE)
                    {
                    printf("HELP NO NEIGHB\n");exit(1);
                    } 
                  }
            
                 printf("next T site is %s %d \n", mol[that_neigh].label, that_neigh);
                 printf("T-O-T bridge is %s %d - %s %d - %s %d\n",
                              mol[replaceable_index[i]].label, 
                              replaceable_index[i],
                              mol[this_neigh].label, this_neigh,
                              mol[that_neigh].label, that_neigh);
                 
                /*****calculate H position *****/
                /*copy triad atoms to simple_atom structure*/
/*****
                T1.coord[1] = mol[replaceable_index[i]].x;
                T1.coord[2] = mol[replaceable_index[i]].y;
                T1.coord[3] = mol[replaceable_index[i]].z;
                T2.coord[1] = mol[that_neigh].x;
                T2.coord[2] = mol[that_neigh].y;
                T2.coord[3] = mol[that_neigh].z;
                bridgeO.coord[1] = mol[this_neigh].x;
                bridgeO.coord[2] = mol[this_neigh].y;
                bridgeO.coord[3] = mol[this_neigh].z;
*****/
                fract_to_cart(&T1.coord[1], &T1.coord[2],&T1.coord[3],
                 mol[replaceable_index[i]].x, 
                 mol[replaceable_index[i]].y,mol[replaceable_index[i]].z,
                 &latt_vec[0]);
                fract_to_cart(&T2.coord[1], &T2.coord[2],&T2.coord[3],
                 mol[that_neigh].x, 
                 mol[that_neigh].y,mol[that_neigh].z,
                 &latt_vec[0]);
                fract_to_cart(&bridgeO.coord[1], 
                 &bridgeO.coord[2],&bridgeO.coord[3],
                 mol[this_neigh].x, 
                 mol[this_neigh].y,mol[this_neigh].z,
                 &latt_vec[0]);

                cation_pos = h_pos(&T1, &T2, &bridgeO);
                /*****change O ****/
                strcpy(mol[this_neigh].label,OH_SPECIES);
                mol[j].charge = -1.426;
                /*****add to list instead of SHEL****/
                /*****assume cores and shells in same order ****/
                j=this_neigh+(num_core_atoms - num_replace_atoms);
                printf("core %s %f %f %f shel %s %f %f %f\n",
                        mol[this_neigh].label,
                        mol[this_neigh].x,mol[this_neigh].y,mol[this_neigh].z,
                        mol[j].label,
                        mol[j].x, mol[j].y,mol[j].z);
                strcpy(mol[j].label, H);
                strcpy(mol[j].type, "core");
                /*convert to frac again*/
                printf("cation pos %f %f %f\n", cation_pos.coord[1],
                     cation_pos.coord[2], cation_pos.coord[3]);
                cart_to_fract(cation_pos.coord[1],cation_pos.coord[2],
                              cation_pos.coord[3],
                              &mol[j].x, &mol[j].y,&mol[j].z,
                              &recip_latt_vec[0]);
                printf("cation pos %f %f %f\n", mol[j].x,mol[j].y , mol[j].z);
                /*
                mol[j].x = cation_pos.coord[1];
                mol[j].y = cation_pos.coord[2];
                mol[j].z = cation_pos.coord[3];
                */
                mol[j].num_neigh = 1;
                mol[j].neighb[0] = this_neigh;
                mol[j].charge = 0.426;
                   
                
               }/*end if (replaceable_done[i] != TRUE) */
             }/*end for i*/
 
           }/*end if (strstr(cation, OH) != NULL)*/
        else
           {
            /**straight cation insertion**/
           }


    

    	/****** write out gulp file ****/
	if ((gulpfp = fopen(gulpfile, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", gulpfile);
            exit(1);
            }
        else
            {
             save_gulpfile(gulpfp, &mol[0],
                           num_atoms, &spacegroup[0], "NONE", &pots_file[0]);
            }
        /*append potentials*/
        sprintf(append_pots_command, "cat %s >> %s\n",
                                           pots_file, gulpfile);
        system (append_pots_command);




/*****DEBUGGGG****/
    	/*** run GULP ***/
        printf("calling gulp...\n");
        sprintf(run_gulp_command,"%s < %s > %s", 
                      RUN_GULP_COMMAND, gulpfile, gulpfile_out); 
        printf("command>> %s\n", run_gulp_command);
    	system(run_gulp_command);


        printf("analysing gulp...\n");
    	/*** read in final energy from GULP ***/
    	final_energy_flag = 0;
        if (strstr(gulp_command_line, SINGLE) != NULL)
         {
    	   initial_energy_flag = 1;
         }
        else
         {
    	   initial_energy_flag = 0;
         }
    	if ((gulpoutputfp = fopen(gulpfile_out, "r")) == NULL)
            	{
            	fprintf(stderr, "Unable to open file %s for output.\n", 
                             	gulpfile_out);
            	exit(1);
            	}
    	else
            	{
			while ((fgets(buffer,BUFFER,gulpoutputfp) != NULL) 
                        	&& (!final_energy_flag))
                   		{
                     		if (strstr(buffer, FINAL_ENERGY) != NULL)
                        		{
                                        if (initial_energy_flag == 0)
                                           {
                                             initial_energy_flag =1;
                                           }
                                        else
                                           {
                         		     final_energy_flag = 1;
                         		     sscanf(buffer, 
                                                    "%*s%*s%*s%*s%lf", 
							&final_energy);
                                           }
                  		         }
                   
                   		}
                }
       
        fclose(gulpoutputfp); 
       /*** insert into list ****/
       /*** did we get an energy **/
       if (final_energy_flag)
          {
           /****WRITE OUT SUBSTITUTION SITE *******/
           save_config(config_fp, this_trial, num_replace_atoms, 
                       final_energy, &replaceable_index[0],
                        &replaceable_done[0]);
           fflush(config_fp);
          }
       else
          {
           /*** if not then skip***/
           printf("Energy not obatined...ignoring\n");
          }

       
   /***debug*****/
   /***save current config as Gulp file***/
   /*
   sprintf(dumpfile, "%s_%s_%d", argv[1],gulpfile, this_trial); 
   if ((dumpfp = fopen(dumpfile, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", dumpfile);
            exit(1);
            }
        else
            {
             sprintf(dummy, "%s_%s_%d", argv[1],gulpfile, this_trial);

             save_gulpfile(dumpfp, &mol[0],
                           num_atoms, &spacegroup[0], &dummy[0], &pots_file[0]);
             fclose(dumpfp);
             sprintf(append_pots_command, "cat %s >> %s\n",
                                           pots_file, dumpfile);
             system (append_pots_command);

            }
   */
   /***save current config as XTL file***/
   /*
   sprintf(dumpfile, "%s_trial_%d.xtl",argv[1], this_trial);
   if ((dumpfp = fopen(dumpfile, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", dumpfile);
            exit(1);
            }
        else
            {
             save_xtlfile(dumpfp, &mol[0],
                           num_atoms, &spacegroup[0]);

            }
    */


   

  }
/***endLOOP****/
fprintf(config_fp, "%s\n", EOF_MARKER);
fprintf(output_fp, "FINISHED\n");
fclose(config_fp);
fclose(output_fp);

/***print out best n configs and generate GULP files for optimisation ***/
}
