/****************************************************************/
/* print_molecule.c : prints a molecule (.car format)           */
/****************************************************************/

#include <stdio.h>
#include "maxima.h"
#include "global_values.h"
#include "structures.h"

void print_molecule(atom *p_molecule, int num_atoms, FILE *output_fp)
{
    int i;
    atom *p_atom;

    printf("Molecule with %d atoms\n", num_atoms);
    for (i = 0; i <= num_atoms; i++)
    {
        p_atom = p_molecule+i;
        fprintf(output_fp, "Atom %d: %s %s %14.9f %14.9f %14.9f %6.3f\n",
                i,
                p_atom->label, p_atom->type,
                p_atom->x,
                p_atom->y, p_atom->z,
                p_atom->charge);
    }
    return;
}
