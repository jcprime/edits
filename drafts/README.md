## Colour key

Colour | LaTeX command | Represents
-------|:-------------:|--------------------------------------------------------------------------------------------------------
Blue   | `\oiface{}`   | Note to act on; question yet to be answered
Olive  | `\edit{}`     | Newly-added stuff to latest draft; in the next draft, they should be back to normal (black) formatting
Grey   | `\hide{}`     | Comments to myself within drafting efforts; when compiled as final effort, they should not be visible
