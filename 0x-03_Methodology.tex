%!TEX root = main.tex

\section{Methodology}
\label{methodology}

% \subsection{Theory}
% \label{methodology:theory}

% IPs

% ES stuff

\input{00_Placeholders}

% Converting GULP (unoptimised) input files into a CP2K-compatible format, writing CP2K input files (using a template and string replacements for ease), and running those as both geometry- and cell-optimisation simulations, using Quickstep module of CP2K, on 4 slots per simulation, on Xenon cluster

\subsection{Simulation of dehydration pathways}
\label{methodology:dehydration_pathways}

Initially, the dehydration pathways of the natrolite family were calculated using the General Utility Lattice Program (GULP),~\cite{Gale2003} with each possible structure at every dehydration step undergoing lattice-energy minimisation at constant pressure (since the volume changes during dehydration were also of interest), using the interatomic potential parameters given in the input files --- and also presented below in \textbf{Section~\ref{methodology:potential_parameters}} --- and then the final minimised energies of each possible structure at the given dehydration step were compared, to find the overall lowest-energy structure, the ``dumped'' output coordinates of which would be used by some custom code, \mono{dehydrate}, to generate the next step's set of possible structures.

The general workflow used, for clarity, is presented in \textbf{Figure~\ref{fig:gulp_workflow}} as a flowchart.  The highest-energy dehydration pathways for the natrolite family were also simulated, using the same procedure as described, except with the sorting of output files by energy reversed.  This allows us to pose alternative pathways but also to identify if these different pathways could be differentiated experimentally.

\begin{figure}
    \centering
    {\footnotesize
    \begin{tikzpicture}[node distance=2cm]
        \node (start) [startstop] {Start};
        \node (io1) [io, below of=start] {Input is the starting, fully-hydrated input file};
        \node (proc1) [process, below of=io1] {Run \mono{dehydrate} on given input file};
        \node (io2) [io, below of=proc1] {One new input file created per water molecule in given (starting) input file, with a different water molecule removed in each one};
        \node (dec1) [decision, below of=io2, yshift=-1cm] {Non-zero number of new input files produced?};
        \node (proc2) [process, below of=dec1, yshift=-1cm] {Run lattice-energy minimisation on each new input file in GULP, in turn};
        \node (io3) [io, below of=proc2] {Optimised output files produced};
        \node (proc3) [process, below of=io3] {Sort output files by final energy};
        \node (io4) [io, below of=proc3] {Take the lowest-energy output file as ground-state structure for that step in dehydration};
        \node (io5) [io, below of=io4] {Copy ``dump''-file (which takes the form of an input file using the optimised configuration) to be the new input for dehydration};
        \node (stop) [startstop, below of=io5] {Stop};
        \coordinate [right of=stop, xshift=6cm] (test) {};
        \draw [arrow] (start) -- (io1);
        \draw [arrow] (io1) -- (proc1);
        \draw [arrow] (proc1) -- (io2);
        \draw [arrow] (io2) -- (dec1);
        \draw [arrow] (dec1) -- node[anchor=east] {yes} (proc2);
        \draw [arrow] (proc2) -- (io3);
        \draw [arrow] (io3) -- (proc3);
        \draw [arrow] (proc3) -- (io4);
        \draw [arrow] (io4) -- (io5);
        \draw [arrow] (io5) -- (stop);
        \draw (dec1) -| node[anchor=south, xshift=-4cm] {no} (test);
        \draw [arrow] (test) -- node[anchor=south] {} (stop);
        % \draw [arrow] (dec1) -- (stop);
    \end{tikzpicture}
    }
    \caption{Flowchart describing the process used to obtain the lowest-energy dehydration pathways using GULP}
    \label{fig:gulp_workflow}
\end{figure}

Calculations were undertaken in GULP with the aim to reproduce previous results~\cite{Gibbs2004} obtained for the dehydration pathways of natrolite and scolecite.  Previous investigations had also partially simulated the dehydration of mesolite, and this investigation so far has both successfully reproduced said results and extended the simulation through mesolite's full dehydration pathway.  The unit cell coordinates for natrolite, scolecite, and mesolite were taken from experimentally-obtained structures.~\cite{Stuckenschmidt1993,Stuckenschmidt1997,Stuckenschmidt2000}

Work is currently underway to extend the level of theory used, simulating the natrolite family using electronic structure methods in CP2K.~\cite{CP2K}

% GULP intro and input file syntax etc

% \input{0x-03_GULP}

% Dehydrate intro, details/workflow, limitations, extensions, and plan

% \input{0x-03_Dehydrate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                   INTERATOMIC POTENTIALS                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Interatomic potential models in GULP~\cite{Gale2003}}
\label{methodology:interatomic_potential_model_used}

% NOTE FROM VIVA 1: Actual minimisation algorithm is apparently ``quasi-Newtonian'', where it uses the first and second derivatives and takes a guess of the starting positions and approximates the Hessian, then uses this to get an estimate of the appropriate step size, and iterates to update not the Hessian but the second derivatives (computationally easier than updating the Hessian) until it achieves convergence
\textbf{Section~\ref{methodology:potential_parameters}} shows the parameters used at this stage of the investigation.  The overall longer-range interactions were calculated in GULP~\cite{Gale2003} using the Ewald summation method,~\cite{Ewald1921} while the short-range interactions were calculated up to cutoff distances between 10 and 20~\r{A}.  The rational function optimisation (RFO) method~\cite{Simons1983,Banerjee1985} was used to converge the overall energy of the system to a minimum, at constant pressure.

\subsubsection*{Intra-framework and \ce{framework-EFC} potentials}
\label{methodology:framework_and_efc_potentials}

The interatomic potentials used to describe the interactions within the zeolite frameworks, and between the framework and the extra-framework cations, were those given by Jackson and Catlow.~\cite{Jackson1988}  They use a shell model for the oxygen ions,~\cite{Jackson1988} which accounts for the polarisation $\alpha$ of the oxygen ions, but also includes a three-body interaction --- of the form $\frac{1}{2} k \left(\theta - \theta_{0}\right)^{2}$, where $k$ is the force constant, $\theta$ is the angle between the three ions in question, and $\theta_{0}$ is the angle between them at equilibrium --- to compensate for the covalent nature of the \ce{Si-Al} distribution.~\cite{Jackson1988}
% , since zeolitic frameworks are not purely ionic structures.

% ~\citet{Jackson1988} found their potentials underestimate Si--O bond lengths.

\subsubsection*{Water potentials}
\label{methodology:water_potentials}

The interactions amongst the water molecules were described by potentials derived by de Leeuw and Parker;~\cite{deLeeuw1998,
% }\textsuperscript{;}\cite{
deLeeuw1999} these were fitted to experimental data on the water monomer and dimer interactions, and also employ a shell model which includes a description of the oxygen ions' polarisation.

The \ce{O_{water}-H} interaction is described by a Morse potential.~\cite{deLeeuw1998}  The lone pairs of the oxygen ions were included by reducing the repulsion of the hydrogen ions.

The water dimer~\cite{Du1994} was used as a basis for the intermolecular water interactions, in which the \ce{O-O} interaction was described using a Lennard-Jones % 12-6
potential, and the intermolecular \ce{O-H} interaction was described using a Buckingham potential.~\cite{deLeeuw1998}
% H charge +0.4e, O overall charge -0.8e (core +1.250e, shell -2.050e)

\subsubsection*{\ce{Water-O_{framework}} potentials}
\label{methodology:water_framework_potentials}

Considering the London dispersion forces that occur between water oxygen and framework oxygen atoms, the $C$ parameter of the Buckingham potential used to describe framework-oxygen-only interactions (cited above) was increased to better account for them.~\cite{deLeeuw1998}

\subsubsection*{\ce{Cation-water} potentials}
\label{methodology:cation_water_potentials}

Since the water oxygens (\ce{O_{water}}) are partially-charged compared to the framework oxygens (\ce{O_{framework}}), their Coulombic interaction with the framework and extra-framework cations would be reduced, as compared to the intra-framework potential parameters.  The \ce{cation-O_{framework}} Buckingham potential was adapted to account for this decrease in charge, by reducing the $A$ parameter for each of the cation-water potentials.~\cite{deLeeuw1998}

\newpage

\subsubsection{Potential parameters}
\label{methodology:potential_parameters}

\begingroup
\renewcommand{\arraystretch}{0.5} % Default value: 1
\begin{longtabu}[c]{lcccc}
\toprule
\toprule
\multicolumn{3}{c}{Charges/$e$} & \multicolumn{2}{c}{\multirow{2}{*}{Core-shell interaction/eV~\r{A}$^{-2}$}} \\\cmidrule{1-3}
Ion & Core & Shell & \multicolumn{1}{l}{} & \\
\midrule
\ce{H^{0.4+}} & 0.40+ & & & \\
\ce{Li^{+}} & 1.00+ & & & \\
\ce{Na^{+}} & 1.00+ & & & \\
\ce{K^{+}} & 1.00+ & & & \\
\ce{Ca^{2+}} & 2.00+ & & & \\
\ce{Mg^{2+}} & 2.00+ & & & \\
\ce{Al^{3+}} & 3.00+ & & & \\
\ce{Si^{4+}} & 4.00+ & & & \\
Framework oxygen~\cite{deLeeuw1998} \ce{O^{2.00-}} & 1.00+ & 3.00\textminus & \multicolumn{2}{d{-1}}{54.80} \\
Water oxygen~\cite{deLeeuw1998} \ce{O^{0.80-}} & 1.25+ & 2.05\textminus & \multicolumn{2}{d{-1}}{209.45} \\
\midrule
\multicolumn{5}{c}{\textbf{Buckingham potential}} \\
Ion pair & $A$/eV & $\rho$/\r{A} & $C$/eV~\r{A}$^{6}$ & Maximum cutoff/\r{A} \\
\midrule
\ce{Si^{4+}-O^{2-}} & 1283.9070 & 0.320520 & 10.662 & 20.000 \\
\ce{Al^{3+}-O^{2-}} & 1460.3000 & 0.299120 & 0.0000 & 20.000 \\
\ce{O^{2-}-O^{2-}} & 22764.000 & 0.149000 & 27.880 & 20.000 \\
\ce{Na^{+}-O^{2-}} & 1226.8400 & 0.306500 & 0.0000 & 20.000 \\
\ce{Ca^{2+}-O^{2-}} & 1090.4000 & 0.343700 & 0.0000 & 16.000 \\
\ce{Li^{+}-O^{2-}} & 1303.9000 & 0.260000 & 0.0000 & 20.000 \\
\ce{K^{+}-O^{2-}} & 1000.3000 & 0.361980 & 10.569 & 20.000 \\
\ce{Mg^{2+}-O^{2-}} & 1428.5000 & 0.294530 & 0.0000 & 20.000 \\
\ce{Al^{3+}-O^{0.8-}} & 1460.3000 & 0.299120 & 0.0000 & 16.000 \\
\ce{Na^{+}-O^{0.8-}} & 450.49800 & 0.306500 & 0.0000 & 20.000 \\
\ce{Li^{+}-O^{0.8-}} & 1303.9000 & 0.260000 & 0.0000 & 20.000 \\
\ce{K^{+}-O^{0.8-}} & 1000.3000 & 0.361980 & 10.569 & 20.000 \\
\ce{Ca^{2+}-O^{0.8-}} & 435.07000 & 0.343700 & 0.0000 & 12.000 \\
\ce{Si^{4+}-O^{0.8-}} & 1283.9070 & 0.320520 & 10.662 & 16.000 \\
\ce{O^{0.8-}-O^{2-}} & 22764.300 & 0.149000 & 28.920 & 16.000 \\
\ce{H^{0.4+}-O^{0.8-}} & 396.27000 & 0.250000 & 10.000 & 20.000 \\ % (inter)
\ce{H^{0.4+}-O^{2-}} & 396.27000 & 0.250000 & 0.0000 & 10.000 \\
\midrule
\multicolumn{5}{c}{\textbf{Morse potential}} \\
Ion pair & D$_{e}$/eV & $\alpha$/\r{A}$^{-1}$ & R$_{0}$/\r{A} & Max Cutoff/\r{A} \\
\midrule
\ce{H^{0.4+}-O^{0.8-}} & 6.2037100 & 2.2200 & 0.92376 & 1.100 \\ % Coulomb subtraction 0.5
\ce{H^{0.4+}-H^{0.4+}} & 0.0000000 & 2.8405 & 1.50000 & 1.650 \\ % Coulomb subtraction 0.5
\midrule
\multicolumn{5}{c}{\textbf{Lennard-Jones (12-6) potential}} \\
\multicolumn{2}{l}{Ion pair} & A/eV~\r{A}$^{12}$ & B/eV~\r{A}$^{6}$ & Max Cutoff/\r{A} \\
\midrule
\multicolumn{2}{l}{\ce{O^{0.8-}-O^{0.8-}}} & 39344.980 & 42.150000 & 20.000 \\
\midrule
\multicolumn{5}{c}{\textbf{Three-body potential}} \\
Ion triplet & \multicolumn{2}{c}{$k$/eV$\cdot$rad$^{-2}$} & \multicolumn{2}{c}{$\theta_{0}$/$^{\circ}$} \\ % & Max Cutoff$_{1-2}$/\r{A} & Max Cutoff$_{1-3}$ /\r{A} & Max Cutoff$_{2-3}$ /\r{A} \\
\midrule
\ce{Al^{3+}-O^{2-}-O^{2-}} & \multicolumn{2}{c}{2.0972} & \multicolumn{2}{c}{109.47000} \\ % & 1.900 & 1.900 & 3.500
\ce{Si^{4+}-O^{2-}-O^{2-}} & \multicolumn{2}{c}{2.0972} & \multicolumn{2}{c}{109.47000} \\ % & 1.900 & 1.900 & 4.000
\ce{O^{0.8-}-H^{0.4+}-H^{0.4+}} & \multicolumn{2}{c}{4.1998} & \multicolumn{2}{c}{108.69000} \\ % Intra; & 1.500 & 1.500 & 2.000
\bottomrule
\bottomrule
\end{longtabu}
% \end{table}
% }
\endgroup
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Just for my own reference: keywords `opti [eigen inten] conp molq rfo [nosym full phonon comp]`, where those in square brackets were used in natrolite input files but not the others

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                ELECTRONIC STRUCTURE METHODS                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Electronic structure methods in CP2K~\cite{CP2K,Hutter2014}}
\label{methodology:cp2k}

The next aim of this investigation was to apply a similar optimisation process to the same materials, using an electronic-structure modelling approach, in order to find out how well the electronic-structure-based calculations fare in comparison to the original interatomic-potential models, both in terms of speed and accuracy, \edit{which we hope will be improved in terms of thermodynamics}.  In an attempt to reduce the initial expense in optimising the geometry, the GULP input files were used to provide starting coordinates at each stage of dehydration.

This stage of the investigation involved using the Quickstep module~\cite{VandeVondele2005} of the CP2K software package,~\cite{CP2K,Hutter2014} to model the natrolite family using the existing GULP~\cite{Gale2003} input files --- starting with the fully-hydrated structures, taken directly from experimental findings --- as a starting point.  Quickstep uses a Gaussian-planewave (GPW) approach~\cite{VandeVondele2005} to model the atomic interactions, rather than the more simple fitted potential functions used in GULP.  The GPW scheme is implemented in Quickstep using localised atom-centred Gaussian-type functions to describe the wavefunctions, and an auxiliary planewave basis set to describe the electron density.~\cite{VandeVondele2005}  Such a combination of basis sets is particularly useful due to the targeted way in which it reduces computational costs for the most expensive calculations --- namely, the orthogonalisation of the wavefunctions and the calculation of the Hartree energy --- without sacrificing accuracy in terms of the remaining calculations.~\cite{VandeVondele2005}

% CP2K input file syntax etc

% \input{0x-03_CP2K}

\subsubsection{Initial simulations of the natrolite family}
\label{methodology:cp2k_natrolites}

The purpose of this part of the investigation was to find an optimal ``intermediate'' hybrid model using CP2K, where the output results concur as much as possible with those obtained previously and with experiment, but that require the least amount of computational ``sacrifice'' to obtain such results.

% At the time of writing, the hydrated natrolite family are being tested using:
% \begin{itemize}
%     \itemsep0em
%     % \item ``DZVP-MOLOPT-SR-GTH'' basis sets;~\cite{Goedecker1996,Guidon2010}
%     \item ``GTH-PBE'' pseudopotentials~\cite{Goedecker1996}
%     \item PBE exchange-correlation functional
%     % \item Self-consistent field (SCF) grid cutoff at 750 \r{A}
%     % \item Relative cutoff at 90 \r{A}, as deduced from cutoff tests
%     \item The orbital-transformation (OT) method,~\cite{VandeVondele2003} in tandem with conjugate gradient (CG) minimisation techniques~\cite{Press1993} to optimise the configuration at every iteration of the SCF loops
%     \item ``FULL\_SINGLE\_INVERSE'' preconditioner
% \end{itemize}

\subsubsection*{Choice of functional and basis sets}
\label{methodology:es_parameters}

Previous work on simulating cation-water clusters has demonstrated the importance of including diffuse functions for oxygen in particular,~\cite{Glendening1995} as is reasonable for anion treatment generally, and so a quadruple-zeta basis set was employed for both water and framework oxygens.

Isolated calcium-water clusters have been quite successfully modelled using a functional including correlation, such as B3LYP, with a triple-zeta split-valence basis set including diffuse functions, such as 6-311+G, for initial geometry optimisations,~\cite{Carl2007} so a similar first attempt in modelling calcium and (water) oxygens for this work is being undertaken for scolecite and mesolite.

Isolated sodium-water clusters have previously been modelled at several levels of theory, which has indicated a need for higher levels of theory to accurately encapsulate the interactions within the larger clusters --- those where five or more water molecules are surrounding the (alkali) metal cation --- such as those including a description of
% \oiface{correlated}
M\o{}ller-Plesset perturbation theory.~\cite{Glendening1995}

% Within zeolitic pores, however, both sodium and calcium cations ``exchange'' their coordination to four water oxygens with framework oxygens, in order to minimise unfavourable steric effects in such comparatively constrained environments.  Two possible approaches could be taken to represent this difference from free clusters, either considering the cations as effectively still larger clusters, and including a correction to compensate for the formal charges of the framework oxygens in place of water molecules, or the cations could be treated as smaller clusters, using levels of theory appropriate for cations coordinated to only two water molecules, and then treated separately for their coordination to the framework.  It seems most reasonable, and potentially least computationally intensive, to treat the cations as described in the former approach, using a higher level of theory where necessary, and including a correction for the discrepancies between the water oxygens' and framework oxygens' respective charges.

% \subsubsection*{Basis sets}
% \label{methodology:basis_sets}

A double-zeta basis set was used to describe most of the components, since initial higher (more diffuse quadruple-zeta) basis sets did not output enough of an appreciable difference in output properties to compensate for the increased runtime.  The specific basis set used was denoted ``DZVP-MOLOPT-SR-GTH''; ``DZVP'' denotes a double-zeta, split-valence basis with polarisation functions included for the $p$ orbitals; ``MOLOPT'' denotes a specific, augmented variant of the ``DZVP-SR-GTH'' basis set that is designed for linearly-scaling calculations,~\cite{VandeVondele2007} which became necessary in response to the repeated failure to converge, as advised by the \href{https://groups.google.com/forum/#!forum/cp2k}{CP2K Google Group}; ``SR'' denotes the ``short-range'' nature of the basis set (i.e.~containing a larger Gaussian exponent than ``longer-range'' basis sets); and ``GTH'' denotes the intention for their use in conjunction with Goedecker-Teter-Hutter pseudopotentials.

% \subsubsection*{Pseudopotentials}
% \label{methodology:pseudopotentials}

% ``GTH-PBE''
Goedecker-Teter-Hutter (GTH) pseudopotentials~\cite{Goedecker1996} were used to represent the core and valence orbitals.  The GTH pseudopotentials describe a separable, norm-conserving set of parameters, optimised for integration of the non-local part over real-space grids.

% \subsubsection*{Exchange-correlation functional}
% \label{methodology:exchange_correlation}

The Perdew-Burke-Ernzerhof (PBE) exchange-correlation functional was used, since it is widely-applicable to many systems.  It is not fitted to any system's empirical data, and so is not dependent on the system to which it is applied.

% \subsubsection*{Self-consistent field calculations}
% \label{methodology:scf}

\oiface{Orbital transformation (OT) method,~\cite{VandeVondele2003} conjugate gradients (CG) minimiser, and \newline ``FULL\_SINGLE\_INVERSE'' preconditioner.  Insert words here.}

% Cutoff tests

\subsubsection*{Cutoff tests}
\label{methodology:cutoff_tests}

The \mono{CUTOFF} keyword in CP2K allows the user to define the planewave cutoff for the finest
level of the multigrid on which the Gaussian functions are mapped.  The \mono{REL\_CUTOFF} keyword controls the mapping itself.
% \oiface{Need to actually explain this in more detail.}
These two properties must be optimised, and the process of doing so --- first by fixing \mono{REL\_CUTOFF}, varying \mono{CUTOFF}, and optimising the energy convergence, then by following the same process with the optimal value of \mono{CUTOFF} fixed, varying \mono{REL\_CUTOFF}, and optimising the energy convergence once more --- is described on the official CP2K website.~\cite{CP2K}

Cutoff tests were performed on the starting natrolite, scolecite, and mesolite input files, and it was found that the optimal \mono{CUTOFF} for natrolite is 800~Ry, for scolecite is 750~Ry, and for mesolite is 280~Ry.  The optimal \mono{REL\_CUTOFF} for natrolite was found to be 50~Ry, for scolecite was found to be 90~Ry, and for mesolite was found to be 40~Ry.

% GULP~\cite{Gale2003} -> CP2K conversions

\subsubsection*{Initial structure coordinates}
\label{methodology:initial_structure_coordinates}

The starting coordinates for each hydrated zeolite were taken from their respective starting GULP~\cite{Gale2003} input files, and converted into CP2K-compatible coordinate files, using a combination of \edit{self-developed} shell and Python scripts to process the fractional input format into Cartesian \mono{xyz} output format.

% Explaining main errors encountered so far

\subsubsection*{Difficulties encountered}
\label{methodology:cp2k_difficulties}

Originally, a higher basis set was used, but a persistent Cholesky decomposition error kept occurring.  Cholesky decomposition takes a positive definite matrix $\mathbf{A}$ (where $\mathbf{A}$ is symmetric, and $\mathbf{x^{T}Ax} > 0$ for all $\mathbf{x} \neq 0$), and factorises it to be equivalent to the product of a lower triangular matrix (with positive diagonal elements) $\mathbf{L}$ and its transpose, $$\mathbf{A} = \mathbf{LL^{T}}.$$

The only permanent solution found for this so far --- following advice given in the CP2K Google Group
% https://groups.google.com/forum/#!searchin/cp2k/cholesky%7Csort:relevance/cp2k/7wfTleKkWsA/YUq8YXU_olsJ
% https://groups.google.com/forum/#!searchin/cp2k/cholesky%7Csort:relevance/cp2k/xguxsCuQsZU/mOU5cLHvnNsJ
--- was to use a lower basis set with ``MOLOPT'' included, as described above.  This was because higher basis sets can cause overcompleteness in the overlap matrix, and the ``MOLOPT'' variants of basis sets were developed to maintain a well-conditioned overlap matrix while including diffuse components.

% Coordinates in CP2K vs. GULP
% \input{0x-03_GULPvsCP2K}

% Reasoning behind ``intermediate'' approach, using GULP starting structures with comparatively simple/``bad'' ES parameters

\subsection{Methodology development (code)}
\label{methodology:development_code}

% \mono{dehydrate} development

% In order to generate the input files for the successive stage of dehydration from a starting file, it was first necessary to identify the water molecules in the starting file, and then iterate through them in leaving them out of the next stage's given input file.

The original version of \mono{dehydrate} was written in C.  It was designed to take a GULP-syntax filename as input, without the file extension, and store the contents of every line of the given file until the \mono{fractional} line was reached.  It would then process the lines after \mono{fractional} one by one, keeping count of the number of water-oxygen (denoted ``O1'') cores to effectively count the number of water molecules in the structure.  It would then calculate which atoms are within bonding distance of one another, and store those groupings.  Finally, it would store the contents of every non-coordinate line after the coordinate section, until the end-of-file is reached.  The rest of its functionality is specific to producing the next set of input files\edit{, i.e.~configurations with a water molecule removed}.

To achieve this, it would iterate through every instance of a detected water molecule, creating a new input file for each one, and to each new file writing out the stored ``start'' lines of the original input file, then writing out the stored contents of the \mono{fractional} section, \emph{but} with the given water molecule commented out, and finally writing out the stored ``end'' lines of the original input file, only replacing instances of the original filename with the new name (with a number appended to the original filename, based on the count of the water molecule being ``removed'').

% Maths of water-molecule identification

Given that in most cases of its use, periodic boundary conditions would be in place, in order to identify which atoms make up the individual water molecules, it is necessary to incorporate the minimum image convention.  The standard bond lengths of common atom pairings are stored in a defined structure, and two given atoms are considered to be bonded if their interatomic separation is less than or equal to the standard bond length plus a set bond tolerance (taken to be 0.1\,\r{A}).

The minimum image convention is used to ensure that the positions of atoms within a certain distance from any given ``edge'' of the bounding box are also considered to be the same distance from the edge on the other side, as if the unit cell has been duplicated on that side for the purpose of that atom's interactions with the atoms in the original unit cell.

% \oiface{Maths of neighbour-finding}

% Limitations
The main limitation of this code is its lack of interoperability with other computational chemistry software formats; \mono{dehydrate} is very dependent on the format of the GULP input file, and so the initial intention for extending it involved writing a CP2K-specific input file processing subroutine.

Work continues to extend the existing code.  The aim is to increase the flexibility of potential inputs, reducing the user's responsibility to provide specific files in such specific formats, and instead developing the code to infer the necessary formatting information itself from the syntax of and\stroke{}or keywords in the input file.

The current updated version takes any filename (and extension) as the input file, and searches its content for either inline coordinates or a path to a coordinate file.  It reads the coordinate lines, in either case (but after first opening the file in the latter!), into an array of strings.  When there are no more lines to be read in, the array is looped through and each atom is classified either as a framework or a solvent atom, and its line is added to a separate array accordingly.  The array of solvent atoms is then looped through, and for each atom, it attempts to find two atoms within bonding distance, and stores their lines in a separate array of molecules.  Once all molecules have been identified, the array of molecules is looped through, and a separate coordinate file is written out with all molecules in order, minus a different one each time, i.e.~all possibilities for the next dehydration step.  The input file is written out again, line by line, with a substitution of the coordinate lines or filename (appended with a distinguishing number, as in the original), and is appended with a distinguishing number in its own filename.

The application of the minimum image convention is something of an issue when considering CP2K output files, as supercells are effectively ``merged'' into one cell when presented in their optimised form, so when taking the ``merged'' output files as input for the next stage in dehydration, the supercell specification must also be extracted from the input file in order to reproduce the original unit cell.

The coordinate-processing functionality is, to a certain degree, relatively simple as compared to GULP file processing, in that most cases will require a separate file to the input file in which to give the coordinates, so the input file is required only to ascertain the path to the coordinate file, the format of the coordinates, supercell settings if present, and the lines to be written to the generated new input files.

The current state of the code is limited in terms of the solvent to be removed, which currently only works for water, and in the coordinate format processing, which currently only works with the default CP2K format (effectively XYZ format).

\subsection{Workflows for input and output file-handling}
\label{workflows}

% Original \mono{GULP} workflow

% \mono{GULP}-to-\mono{CP2K} conversion workflow

\edit{The initial manual pre-processing stage required the creation of a working CP2K input file for the fully-hydrated zeolite under consideration, and the conversion of this specific input file into a template file.  The template file would itself then be used as the basis for converting each existing GULP input file into a CP2K input file, simply by replacing the placeholder values in the template file with the respective specific data from the GULP input file.}

Scripts were written, in bash and Python, to extract and convert the fractional coordinates from the GULP input files into a Cartesian coordinate format, which is then used as a separate file included by name in the main CP2K input file.  This coordinate file is updated with the new coordinates as they change.

% \mono{GULP}-\emph{informed} \mono{CP2K}-\emph{generated} workflow

Currently, the pathways are being recreated using a still-faster method, aiming to use CP2K for the full process, optimising the geometry at each stage as before, but using the existing GULP-predicted pathway to inform which water molecule is lost at each stage; that molecule will then be removed manually from the coordinate file, and so on until no water molecules remain.  \edit{A flowchart detailing the workflow for this method is shown in \textbf{Figure~\ref{fig:gulp_informed_workflow}}.  This will be tested for speed and accuracy against the method outlined above, which converted every GULP input file into a CP2K-compatible format.}

\edit{So, rather than the exhaustive search performed in GULP, the GULP-predicted pathway is followed with the higher-level model in CP2K, thus speeding the process considerably by eliminating all but the lowest-energy possible configurations from the original interatomic-potential-based simulations at each dehydration step.}

\begin{figure}
    \centering
    {\footnotesize
    \begin{tikzpicture}[node distance=2cm]
        \node (start) [startstop] {Start};
        \node (io1) [io, below of=start] {Input is the starting, fully-hydrated input file, taken from the GULP input file that has been converted to a CP2K input file};
        \node (proc1) [process, below of=io1] {Run given input file in CP2K (cell optimisation)};
        \node (io2) [io, below of=proc1] {Obtain optimised structure as ``restart'' (CP2K-generated input) file};
        \node (proc2) [process, below of=io2] {Note which water molecule is removed in the lowest-energy structure of the next dehydration step, as calculated by GULP};
        \node (proc3) [process, below of=proc2, yshift=-0.3cm] {Manually remove said water molecule from a \emph{copy} of the given restart file, and rename the copy to reflect the new dehydration step};
        \node (dec1) [decision, below of=proc3, yshift=-1.2cm] {Any water molecules remaining?};
        \node (proc4) [process, below of=dec1, yshift=-1cm] {Run final, fully-dehydrated structure's input file in CP2K (cell optimisation)};
        \node (io3) [io, below of=proc4] {Obtain optimised fully-dehydrated structure as ``restart'' file};
        \node (stop) [startstop, below of=io3] {Stop};
        \coordinate [right of=proc1, xshift=4cm] (test) {};
        \draw [arrow] (start) -- (io1);
        \draw [arrow] (io1) -- (proc1);
        \draw [arrow] (proc1) -- (io2);
        \draw [arrow] (io2) -- (proc2);
        \draw [arrow] (proc2) -- (proc3);
        \draw [arrow] (proc3) -- (dec1);
        \draw [arrow] (dec1) -- node[anchor=east] {no} (proc4);
        \draw [arrow] (proc4) -- (io3);
        \draw [arrow] (io3) -- (stop);
        \draw (dec1) -| node[anchor=east] {} (test);
        \draw [arrow] (test) -- node[anchor=south] {yes} (proc1);
    \end{tikzpicture}
    }
    \caption{Flowchart describing the process used to obtain the lowest-energy dehydration pathways in CP2K, using GULP only to inform the order of water removal}
    \label{fig:gulp_informed_workflow}
\end{figure}

\newpage
