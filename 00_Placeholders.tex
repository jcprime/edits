%!TEX root = main.tex

There are two general approaches to describe the energy of a system: interatomic potential methods, which calculate the overall forces acting between atoms in the system, and electronic structure methods, which involve the application of quantum mechanics to the system.

\subsection{Interatomic potential methods}
\label{theory:interatomic_potentials}

Interatomic potential (IP) models make use of functions of a system's nuclear positions, which together provide an overall energy for the system.~\cite{Catlow_CMoMM_BOOK}  Several types of simulation can make use of these models, each providing a different focus for their calculations.  For example, Monte Carlo simulations,~\cite{Hastings1970,Kroese2014} using probability distributions and the Metropolis algorithm,~\cite{Metropolis1953,Liu2000,Martino2013} can give details about ensemble averages,~\cite{Catlow_CMoMM_BOOK} whereas molecular dynamics simulations,~\cite{Fermi1955} using equations to incorporate motion into their calculations, can give details on the specific mechanisms involved in reaching a given outcome, such as a final energy-minimised structure.~\cite{Catlow_CMoMM_BOOK}
% \oiface{TK: EQUATION FOR MD}

% FROM MSCI REPORT
The potential free energy of a given system can be calculated by way of a summation over the full range of interatomic interactions within that system.  The long-range forces are a result of the electrostatic, or Coulombic, interaction between ions, calculated as follows:
$$U_{Coul}(r) = \frac{1}{2}\sum{\frac{q_{i}q_{j}}{4\pi\varepsilon_{0}r_{ij}^{2}}},$$
where $U_{Coul}(r)$ is the electrostatic interaction, $q_{i}$ is the charge of atom $i$, $r_{ij}$ is the distance between atoms $i$ and $j$, and $\varepsilon_{0}$ is the electric permittivity of free space.  Coulomb's constant $k_{e}$ is equivalent to $\frac{1}{4\pi\varepsilon_{0}}$.
The short-range forces at very small interatomic separations are primarily repulsive, acting mutually between the electron density of neighbouring atoms or ions, which dominate at very short interatomic separations, but rapidly decrease as the separation increases, at which point the (attractive) van der Waals' dispersion forces and attractive electrostatic forces dominate.

The combination of short- and long-range forces, per pair of atoms or ions in the system, can be accounted for in various ways, with varying degrees of accuracy.  The summation over all components of the system provides the total energy of that system. % Since the potential (energy) $\mathbf{V} = \int \mathbf{F} \mathrm{d}r$, and acceleration (just for the sake of it) $\mathbf{a} = \frac{\D \mathbf{x}}{\D t} = \frac{\D^{2} x}{\D t^{2}}$.

\subsubsection{Potential functions}
\label{theory:interatomic_potential_functions}

\subsubsection*{The Lennard-Jones potential}
\label{theory:lennard_jones}

The Lennard-Jones potential\cite{LennardJones1924} is a relatively simple expression for the interaction between two atoms as a function of their separation, given by
$$U_{LJ}(r_{ij}) = \epsilon\left[ \left( \frac{r_{m}}{r_{ij}} \right)^{12} - 2\left( \frac{r_{m}}{r_{ij}} \right)^{6} \right],$$
where $\epsilon$ is the depth of the potential well, $r_{m}$ is the distance at which the (pair) potential curve reaches its minimum value, and $r_{ij}$ is the interatomic separation $r$ between atoms $i$ and $j$.  Its alternative form is given by
$$U_{LJ} (r_{ij}) = \frac{A}{r_{ij}^{12}} - \frac{B}{r_{ij}^{6}},$$
where $r_{ij}$ is as above, and $A$ and $B$ are constants equivalent to $\epsilon (r_{m}^{12})$ and $2\epsilon (r_{m}^{6})$, respectively.
The positive $r^{-12}$ term in the Lennard-Jones equation corresponds to the dominant electrostatic repulsive force at very short separations, and the negative $r^{-6}$ term to the combined attractive forces.

\subsubsection*{The Buckingham potential}
\label{theory:buckingham}

The Buckingham potential~\cite{Buckingham1938} is an approximation of the Lennard-Jones potential, in which the repulsive term is considered to be exponential:
$$U_{Buck} (r_{ij}) = A{\cdot}\exp{\left(-\frac{r_{ij}}{\rho}\right)} - \frac{C}{r_{ij}^{6}}$$
where $r_{ij}$ is as above, $A$ represents the ion size, $\rho$ represents the ion hardness, and $- \frac{C}{r_{ij}^{6}}$ represents the dispersive forces.

\subsubsection*{The Morse potential}
\label{theory:morse}

The Morse potential~\cite{Morse1929} is of the form:
$$U_{Morse}(r) = D_{e}\left(1 - e^{(- \alpha (r_{ij} - r_{e}))}\right)^{2},$$
where $D_{e}$ is the depth of the potential well, $r_{ij}$ is the interatomic separation, $r_{e}$ is the equilibrium separation between atoms $i$ and $j$, and $\alpha$ is a parameter controlling the width of the potential well, given by $\sqrt{\frac{k_{e}}{2 D_{e}}}$, where $k_{e}$ is the force constant at the equilibrium separation.

% NOTE: In deLeeuw1998, the OH potential is of the form: $$\Phi_{ij}(r_{ij}) = D (1 - \exp{[- \alpha (r_{ij} - r_{0})]})^{2} - D$$ where $D$ is the bond dissociation energy, $r_{ij}$ is the interatomic separation, and $r_{0}$ is the equilibrium separation between atoms $i$ and $j$, and $\alpha$ is the polarisation, which is fitted to experimental IR frequencies.

\subsubsection*{The pair potential curve}
\label{theory:pair_potential_curve}

The long-range Coulomb potential is effectively an $r^{-2}$ curve, the van der Waals attraction an $r^{-6}$ curve, and the Pauli repulsion an $r^{-12}$ curve in the Lennard-Jones equation, or an $\exp({-r^{-1}})$ curve in the Buckingham equation.  To understand the resultant interactions between two particles, these each must be evaluated and combined over all $r$.

Evaluating the sum of attractive and repulsive forces over all $r$ allows an overall potential energy function to be constructed, in terms of the interatomic separation $r$ over a given range --- preferably one which extends beyond the domain of short-range interactions to allow visualisation of the long-range interactions tapering off.  Considering repulsion as positive, and attraction as negative, the pair potential curve takes a specific shape, as shown in \textbf{Figure~\ref{fig:pair_potential_curve}}.

\begin{figure}[ht]
    \input{../bin/stats/phd/output/figs/pair_potential_curve.tex}
    % \includegraphics[width=0.5\textwidth]{graphics/PlaceholderImage.jpg}
    \caption{Typical shape of the pair potential curve in dimensionless units, shown as a solid line, with the repulsive (dotted) and attractive (dashed) components shown individually}
    \label{fig:pair_potential_curve}
\end{figure}

\subsubsection{Ewald summation}
\label{theory:ewald_summation}

For systems involving more than two atoms, the overall energy of the system must be evaluated by combining the effects of interactions between pairs of atoms over the entirety of the system.  The Ewald summation method involves a real-space calculation of the overall short-range interactions, combined with a Fourier-space calculation of the overall long-range interactions.~\cite{Ewald1921}

\subsubsection{Interatomic potential models}
\label{theory:interatomic_potential_models}

\subsubsection*{Rigid ion models}
\label{theory:rigid_ion_models}

Rigid-ion models consider atoms and ions as being hard spheres with formal charges, and so cannot account for polarisability of the electron shells.  They are only appropriate models to use for primarily-ionic systems.

\subsubsection*{Shell models}
\label{theory:shell_models}

The shell model treats the outermost electron shell as a separate entity from the core and inner electron shells, with the former being a ``moveable'' sphere, and the latter being effectively ``rigid'', and the core-shell interaction is described by that of a harmonic spring, with force constant $k$, as follows: $\Phi_{i} (r_{i}) = \frac{1}{2} k_{i} r_{i}^{2}$ (where $r_{i}$ is the separation between the centres of the core and shell), which accounts for the (particularly an-)ionic polarisability where a ``rigid-ion'' model would not.

The charge on the shell, and force constant $k$, for the ion under consideration are determined by the static dielectric constant $\epsilon$ and its frequency dependence.~\cite{Dick1958}

\subsection{Electronic structure methods}
\label{theory:electronic_structure}

A more accurate set of ``electronic structure'' methods, such as density functional theory-based methods, employ techniques designed to solve the \Schrodinger{} equation, to a certain degree of accuracy.  In its most general form, the \Schrodinger{} equation describes the state of a system in terms of wavefunctions $\psi$:
$$E\psi = \hat{H}\psi,$$
where $\hat{H}$ is the Hamiltonian operator, which is dependent on the system in question.  These wavefunctions can form standing waves, corresponding to atomic (or molecular) orbitals $\phi_{i}$.

Essentially, by attempting to solve this equation for the many-body problem, using various approximations, electronic structure (ES) techniques aim to provide more accurate structural predictions than can be obtained from interatomic potentials alone.

\subsubsection{Approximations}
\label{theory:approximations}

\subsubsection*{Born-Oppenheimer approximation}
\label{theory:born_oppenheimer}

The Born-Oppenheimer approximation relates to the speed of motion of electrons compared with nuclei.  Since nuclei are comparatively much more massive than electrons, they move so slowly as to be considered effectively stationary, and the potential generated by their positive charge can thus be considered static.

\subsubsection*{Hartree-Fock approximation}
\label{theory:hartree_fock}

The Hartree-Fock (HF) approximation aims to simplify the \Schrodinger{} equation by breaking down the representation of complex orbitals into multiple one-electron orbitals that can later be combined.

\subsubsection*{Basis sets}
\label{theory:basis_stuff}

Basis sets are used to describe the wavefunctions of a system; they are composed of basis functions, each of which represents an atomic orbital in the system, and linear combinations of basis functions represent molecular orbitals.

Plane wave basis sets can also be used as an alternative to atomic orbital representations, and are particularly applicable to systems with periodic boundary conditions.

\subsubsection*{Pseudopotentials}
\label{theory:pseudopotentials}

Given that the valence electrons of a system's component atoms are the main controllers of its ground-state properties, calculations incorporating the often-larger number of core electrons into the ``full'' potential spend a lot of their overall time and resources on accounting for comparatively small effects.  Pseudopotentials approximate the effect of the core electrons as an effective potential, and combine those effective potentials with the valence wavefunctions, to produce a far simpler overall description of the atom, which is much less computationally intensive to use in simulations than a ``full'' potential would be.

% The use of pseudopotentials assumes there is no significant overlap between the core and valence electrons; if there is, pseudopotentials are not appropriate for use to describe that particular system.

\subsubsection*{Exchange-correlation functionals}
\label{theory:xc_functionals}

% Note: electron exchange is also known as Fermi correlation, as it prevents two electrons with parallel spin being found at the same point in space; Coulomb correlation describes the correlation between the spatial positions of electrons due to their Coulombic repulsion, which is responsible for things like London dispersion forces.

% Hartree-Fock methods fail to account for the Coulomb correlation of the wavefunction.
% % ... because they approximate the wavefunction using a single Slater determinant, which is insufficient to achieve exact...ness!
The electron-correlation energy of a system accounts for the interaction between electrons in the system, while the electron-exchange (also known as the Fermi correlation) energy accounts for the specific interaction between electrons with parallel spin.  Many methods include the exchange energy to some extent, but fewer early methods incorporated the Coulombic correlation energy in any form, and it is this latter energy that exchange-correlation functionals are primarily designed to include in more advanced methods.

% \oiface{LDA, GGA, etc.}
There are two main ``classes'' of functionals for describing the exchange-correlation (XC) energy, which are differentiated by the way in which they consider the energy to depend on electron density.  Local density approximation (LDA) functionals, as their name suggests, take the XC energy to be equivalent to the XC energy of a uniform electron gas (jellium) of the same density at the given point, and thus to be local to the point itself.  Generalised gradient approximation (GGA) functionals incorporate ``non-local'' --- i.e.~higher-order --- parameters, such as the gradient of the density at the given point, into the XC energy calculation.

\subsubsection{Methods}
\label{theory:esmethods}

\subsubsection*{Hartree-Fock methods}
\label{theory:hf}

Hartree-Fock methods employ an independent-particle model, which uses the average interaction across all electrons to be representative of each electron-electron interaction, and neglect the Coulomb correlation energy of the given system.

The wavefunction is taken to be approximated by a Slater determinant \oiface{DEFINE?}, where there includes one orbital representation per electron; the variational principle is applied to the orbital representations in this determinant, and the determinant with the lowest energy output is taken to be the most accurate representation of the wavefunction within the limitations of the approximation.  \textbf{Note that the orbitals must remain orthonormal throughout the process, which is achieved using Lagrange's method of undetermined multipliers.}

% \subsubsection*{Post-Hartree-Fock methods}
% \label{theory:post-hf}

\oiface{Words continued...}

\subsubsection*{Density functional theory}
\label{theory:dft}

Density functional theory (DFT) methods include the Born-Oppenheimer approximation, and consider the overall energy of a system to be a function of the electron density, which is itself a function of the atomic positions: $E[n(r)]$.  Given a set level of accuracy to achieve, these techniques aim to minimise the energy functional, by first guessing the initial density $n$, then calculating the atomic orbitals using the Kohn-Sham equations,~\cite{KohnSham1965} and refining the density iteratively until the required accuracy level is achieved.

% \subsection{Energy minimisation}
% \label{theory:energy_minimisation}

\subsection{Local optimisation techniques}
\label{local_optimisation}

The mathematical basis for any structural optimisation involves the interpretation of the overall internal energy of any chemical system as an infinite Taylor series, thus:
\begin{equation*}
U(x + \delta x) = U(x) + \frac{\partial U}{\partial x} \delta x + \frac{1}{2!} \frac{\partial^{2}U}{\partial x^{2}} (\delta x)^{2} + \frac{1}{3!} \frac{\partial^{3}U}{\partial x^{3}} (\delta x)^{3} + \cdots + \frac{1}{n!} \frac{\partial^{n}U}{\partial x^{n}} (\delta x)^{n},
\end{equation*}
where the first derivative, $\frac{\partial U}{\partial x}$, is known as the gradient vector $g$, and the second derivatives, $\frac{\partial^{2}U}{\partial x^{2}}$, are constructed into the Hessian matrix $\mathbf{H}$, which, in its most general form, is given by:
$$\mathbf{H} = \left[
\begin{matrix}
    \frac{\partial^{2}f}{\partial x^{2}_{1}} & \frac{\partial^{2}f}{\partial x_{1} x_{2}} & \cdots & \frac{\partial^{2}f}{\partial x_{1} x_{n}} \\
    \frac{\partial^{2}f}{\partial x_{2} x_{1}} & \frac{\partial^{2}f}{\partial x^{2}_{2}} & \cdots & \frac{\partial^{2}f}{\partial x_{2} x_{n}} \\
    \vdots & \vdots & \ddots & \vdots \\
    \frac{\partial^{2}f}{\partial x_{n} x_{1}} & \frac{\partial^{2}f}{\partial x_{n} x_{2}} & \cdots & \frac{\partial^{2}f}{\partial x^{2}_{n}} \\
\end{matrix}
\right]$$
for a function $f(\mathbf{x})$ with $\mathbf{x} = (x_{1}, x_{2}, \cdots, x_{n})$.  It is this latter matrix that most modern optimisation methods update iteratively until convergence is achieved.

The mechanism by which the Hessian is updated is the main way of distinguishing between different local-optimisation algorithms, although some only use the first derivatives as a means of optimisation.

In the latter case, the first derivatives are used to infer the direction of ``movement'', and a line search is undergone to determine the step size for the next iteration.  The ``steepest descent'' method simply continues to iterate until the required conditions for convergence are met, and thus makes no use of the information already gained from earlier iterations, while the more efficient ``conjugate gradients'' method utilises orthogonality of later steps' vectors with those preceding them.

The former case is somewhat more complex, but also more rigorous.  From any step in the optimisation process, the minimum can be found as follows:
\begin{equation*}
\Delta x = - \alpha H^{-1}g,
\end{equation*}
where $\Delta x$ is the displacement vector from the given step to the minimum-energy configuration, and $\alpha$ is determined by line search.  This presumes the use of Newton-Raphson methods, which calculate increasingly-accurate solutions to the given function --- in this case, that of the internal energy in terms of the atomic coordinates --- from the calculated value of the previous iteration and the function's and its derivative's values at that point.  These iterations can be generalised thus:
\begin{equation*}
x_{n+1} = x_{n} - \frac{f(x_{n})}{f'(x_{n})},
\end{equation*}
and generally provide approximate solutions for real-world systems in simulation, and allow to some extent for the proximity of the starting point to local maxima or transition-state configurations.

\subsubsection{Updating the Hessian}
\label{updating_hessian}

Inverting the Hessian is the most expensive step in the iterative optimisation process, but the change in the Hessian between steps can often be comparatively small.  It is therefore useful to avoid unnecessarily repeating the inversion at every step; instead, some methods use alternatives --- such as the changes in the gradient and variables, which are computationally less expensive to track than the Hessian itself --- to ``update'' the Hessian in an approximate sense, compared to the form it would take, were it updated exactly.  Two main updating techniques are used: Davidson-Fletcher-Powell~\cite{Fletcher1963} (DFP), and Broyden-Fletcher-Goldfarb-Shanno~\cite{Shanno1970} (BFGS), with BFGS generally acknowledged as the more efficient scheme unless looking for a transition state.

Newton-Raphson methods are generally sufficient to obtain convergence from points approaching the required stationary point, whereas conjugate gradient methods are more robust to the starting point, and thus more appropriate for larger systems.

% \subsubsection{Convergence criteria}
% \label{convergence_criteria}

% Gnorm (gradient norm = root mean square gradient per variable)
% Max individual gradient component
% Norm of estimated displacement vector
% Change in the function (energy, enthalpy, or free energy) between successive cycles
% Change in variables between successive cycles

In the case that the second derivative matrix is not positive, the situation becomes more complex with regards to optimisation.  Algorithms designed to find transition states or local maxima can also use Newton-Raphson techniques, except with the focus instead on stationary points with a Hessian composed of a given number of negative eigenvalues.

%The stationary-point locating algorithm used in GULP is the rational functional optimisation (RFO) method.~\cite{Banerjee1985}

\subsubsection*{Rational functional optimisation~\cite{Banerjee1985}}
\label{rfo}

This method involves diagonalising the inverse Hessian, which provides the eigenvalues and eigenvectors required to analyse at each step whether or not the desired type of stationary point has been reached.  The next step's search direction is altered, based either on the smallest eigenvalue's mode
% DEFINE ``MODE'' PLEASE!
or the eigenvectors of the Hessian, but transition states are particularly difficult to locate, so the step size for such searches must be sufficiently small so as not to ``jump'' past them in the process.
